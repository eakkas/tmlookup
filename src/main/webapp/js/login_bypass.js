function decodeBase64(s) {
		if (s == undefined) {
			return s;
		}
		var e = {}, i, b = 0, c, x, l = 0, a, r = '', w = String.fromCharCode, L = s.length;
		var A = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		for (i = 0; i < 64; i++) {
			e[A.charAt(i)] = i;
		}
		for (x = 0; x < L; x++) {
			c = e[s.charAt(x)];
			b = (b << 6) + c;
			l += 6;
			while (l >= 8) {
				((a = (b >>> (l -= 8)) & 0xff) || (x < (L - 2))) && (r += w(a));
			}
		}
		return r;
	};

	function getUrlParameter(param) {

		var pageURL = window.location.search.substring(1);
		var params = pageURL.split("&");
		for ( var i = 0; i < params.length; i++) {
			var name = params[i].split("=");
			if (name[0] == param) {
				return name[1];
			}
		}
	}
	$(document)
			.ready(
					function() {

						var u = decodeBase64(getUrlParameter('u'));
						var p = decodeBase64(getUrlParameter('p'));
						if (u == undefined || p == undefined) {
							return;
						}
						$("#j_username").attr("value", u);
						$("#j_password").attr("value", p);
						// this is the id of the form
						$("#loginForm")
								.submit(
										function() {

											var url = "${pageContext.request.contextPath}/j_spring_security_check"; // the
											$
													.ajax({
														type : "POST",
														url : url,
														data : $("#loginForm")
																.serialize(),
														success : function(data) {
															if (data
																	.indexOf("Wrong login or password") != -1) {
																alert("Invalid username/password. Please contact administrator");
																window.location = "${pageContext.request.contextPath}/login";
															} else {
																window.location = "${pageContext.request.contextPath}";
															}
															console.log(data);

														},
														error : function(err) {
															console.log(err);
														}

													});

											return false; // avoid to execute the
											// actual submit of the
											// form.
										});

						var event = jQuery.Event("submit");
						$("#loginForm").trigger(event);
						if (event.isDefaultPrevented()) {
							// Perform an action...
						}
					});