<!DOCTYPE HTML>
<%-- @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt; --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Globalme TM Lookup Tool</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/js/libs/jquery/jquery-1.8.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/main.js"></script>
    <script src="${pageContext.request.contextPath}/js/json2.js"></script>
</head>
<body>
<%-- <%@ include file="include/feedbackDialog.jsp" %> --%>
<jsp:include page="include/emptyNavBar.jsp" flush="true"/>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="span12">
                <div class="button-container">
                    <a href="tb">
                        <button class="button-rectangle span3">Terminology<br> Search</button>
                    </a>

                    <div class="span2"></div>
                    <a href="tm">
                        <button class="button-rectangle span3">Translation database<br> Search</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <%@ include file="include/footer.jsp" %>
</div>
</body>

</html>