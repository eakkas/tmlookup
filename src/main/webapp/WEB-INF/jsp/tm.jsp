<%@ page import="net.globalme.gstestingtool.machinetranslation.entity.Locale" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<!DOCTYPE html>
<%-- @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt; --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Globalme TM Lookup Tool</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/js/libs/jquery/jquery-1.8.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/main.js"></script>
    <script src="${pageContext.request.contextPath}/js/json2.js"></script>
</head>
<body>
<%-- <%@ include file="include/feedbackDialog.jsp" %>
 --%><%@ include file="include/editTmDialog.jsp" %>
<jsp:include page="include/navBar.jsp" flush="true">
    <jsp:param name="active" value="tm"/>
    <jsp:param name="name" value="${name}"/>
</jsp:include>
<div class="container">
    <div class="content">
        <div class="page-header">
            <h1>Translation Memory Search</h1>
        </div>

        <div class="row">
            <div class="span12">
                <div class="alert alert-error hide" id="validationErrorAlert">
                    <strong>Error!</strong>
                </div>
                <form class="form" autocomplete="off" action="javascript:void(0);">
                    <div class="controls controls-row">
                        <label class="span6">Text to search</label>
                        <label class="span2">Source language</label>
                        <label class="span2">Target language</label>
                    </div>
                    <div class="controls controls-row">
                        <input id="textTmSearch" type="text" class="span6"/>
                        <select id="selectTmSource" class="span2">
                            <% final Map<Locale, ArrayList<Locale>> locales = (Map<Locale, ArrayList<Locale>>) request.getAttribute("locales");
                                Locale selectedLocale = null;
                                for (final Locale locale : locales.keySet()) {
                                    if (selectedLocale == null) {
                                        selectedLocale = locale;
                                    }%>
                            <option value="<%=locale.getCode()%>"><%=locale.getDisplayName()%>
                            </option>
                            <%}%>
                        </select>
                        <select id="selectTmTarget" class="span2">
                            <%
                                if (selectedLocale != null) {
                                    for (final Locale locale : locales.get(selectedLocale)) {%>
                            <option value="<%=locale.getCode()%>"><%=locale.getDisplayName()%>
                            </option>
                            <%
                                    }
                                }
                            %>
                        </select>

                        <div class="span1">
                            <button id="buttonTmSearch" class="btn btn-primary button-search">Search</button>
                        </div>
                    </div>
                </form>
                </br>
                <div id="results" class="hide">
                    <table class="table table-striped table-hover" id="tableTmResults">
                        <h3>Results</h3>
                        <thead>
                        <tr>
                            <th class="span5">Source</th>
                            <th class="span5">Translation</th>
                            <th class="span2">Locale</th>
                            <th class="span2">Percentage</th>
                            <th class="span2"></th>
                        </tr>
                        </thead>
                        <tbody id="tableBodyTmResults">
                        </tbody>
                    </table>
                </div>
                <div id="noResults" class="hide">
                    <h3>No matches found</h3>
                </div>

                <div id="additionalResults" class="hide">
                    <table class="table table-striped table-hover" id="tableAdditionalTmResults">
                        <tbody id="tableBodyAdditionalTmResults">
                        </tbody>
                    </table>
                </div>
                <div class="pull-right showAdditionalResultsButtonContainer hide"
                     id="showAdditionalResultsButtonContainer">
                    <a href="#"
                       onclick="$('#showAdditionalResultsButtonContainer').hide();$('#additionalResults').show('slow');">
                        + Show matches for all languages</a>
                </div>
            </div>
        </div>
    </div>
    <%@ include file="include/footer.jsp" %>
</div>
<script type="text/javascript" charset="utf-8">
    function showError(message) {
        var elementValidationError = $('#validationErrorAlert');
        elementValidationError.addClass('show');
        elementValidationError.html('<strong>Error: </strong> ' + message);
    }

    function showEditDialog(source, sourceLocale, target, targetLocale) {
        $('#editTmDialogSource').val(source);
        $('#editTmDialogSourceLocale').val(sourceLocale);
        $('#editTmDialogTargetLocale').val(targetLocale);
        if (target.length > 30) {
            $('#editTmDialogTranslationInputControl').hide();
            $('#editTmDialogTranslationTextareaControl').show();
            $('#editTmDialogTranslationTextarea').val(target);
            $('#editTmDialogTranslationInput').val('');
        } else {
            $('#editTmDialogTranslationInputControl').show();
            $('#editTmDialogTranslationTextareaControl').hide();
            $('#editTmDialogTranslationInput').val(target);
            $('#editTmDialogTranslationTextarea').val('');
        }

        $('#editTmDialogDialog').modal('show');
    }

    function hideAllResults() {
        $('#noResults').hide();
        $('#results').hide();
        $('#additionalResults').hide();
        $('#showAdditionalResultsButtonContainer').hide();
    }

    function fillTablesWithResults(data) {
        var elementTableBodyResults = $('#tableBodyTmResults');
        var elementTableBodyAdditionalResults = $('#tableBodyAdditionalTmResults');
        elementTableBodyResults.html('');
        elementTableBodyAdditionalResults.html('');

        var targetMatchesNumber = 0;
        for (var i = 0; i < data.length; i++) {

            var targetLanguage = $('#selectTmTarget').val();

            if (data[i].locale == targetLanguage) {
                elementTableBodyResults.append('<tr><td>' + data[i].source + '</td><td>' + data[i].translation +
                        '</td><td>' + data[i].locale + '</td><td>' + data[i].percentage +
                        '</td><td><a onclick="showEditDialog(\'' + data[i].source +
                        '\', \'' + data[i].sourceLocale + '\', \'' + data[i].translation + '\', \'' + data[i].locale +
                        '\')">edit</a></td></tr>');
                targetMatchesNumber++;
            } else {
                elementTableBodyAdditionalResults.append('<tr><td class="span5">' + data[i].source +
                        '</td><td class="span5">' + data[i].translation + '</td><td class="span2">' +
                        data[i].locale + '</td><td class="span2">' + data[i].percentage +
                        '</td><td class="span2"><a onclick="showEditDialog(\'' + data[i].source +
                        '\', \'' + data[i].sourceLocale + '\', \'' + data[i].translation + '\', \'' + data[i].locale +
                        '\')">edit</a></td></tr>');
            }
        }
        if (targetMatchesNumber != data.length) {
            $('#showAdditionalResultsButtonContainer').show();
        } else {
            $('#showAdditionalResultsButtonContainer').hide();
        }
    }

    function onSearchSuccess(data) {
        hideAllResults();

        var elementResults = $('#results');
        var elementNoResults = $('#noResults');

        if (data.length == 0) {
            elementResults.hide();
            elementNoResults.show('slow');
            return;
        }

        fillTablesWithResults(data);

        elementNoResults.hide();
        elementResults.show('slow');
    }

    var locales = {};
    <%for (final Locale sourceLocale : locales.keySet()) {%>
    locales['<%=sourceLocale.getCode()%>'] = [];
    <%for(final Locale targetLocale : locales.get(sourceLocale)){%>
    locales['<%=sourceLocale.getCode()%>'][<%=locales.get(sourceLocale).indexOf(targetLocale)%>] =
    {'code': '<%=targetLocale.getCode()%>', 'displayName': '<%=targetLocale.getDisplayName()%>'};
    <%}
    }%>

    var elementSelectTmSource = $('#selectTmSource');
    elementSelectTmSource.change(function () {
        var elementSelectTmTarget = $('#selectTmTarget');
        elementSelectTmTarget.html('');
        for (var i = 0; i < locales[elementSelectTmSource.val()].length; i++) {
            elementSelectTmTarget.append('<option value="' + locales[elementSelectTmSource.val()][i].code +
                    '">' + locales[elementSelectTmSource.val()][i].displayName + '</option>');
        }
    });

    $('#buttonTmSearch').live('click', function (e) {
        $('#validationErrorAlert').hide();

        var query = $('#textTmSearch').val();
        if (query == 0) {
            showError('Search query is empty');
            return;
        }

        $('#buttonTmSearch').button('loading');

        var sourceLanguage = $('#selectTmSource').val();
        var targetLanguage = $('#selectTmTarget').val();
        if (sourceLanguage == targetLanguage) {
            showError('Source language is equal to target language');
            return;
        }

        $.ajax({
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            url: 'api/gs/tm?q=' + query + '&from=' + sourceLanguage,
            success: function (data) {
                $('#buttonTmSearch').button('reset');
                onSearchSuccess(data)
            },
            error: function (error) {
                $('#buttonTmSearch').button('reset');
                showError(error.status + '');
            }
        });
    });
</script>
</body>
</html>