<%-- @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt; --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>

<div id="editTbDialogDialog" class="modal hide fade in" style="display: none; ">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>

        <h3>Edit TB Entry</h3>
    </div>
    <div class="modal-body">
        <div class="alert alert-error hide" id="editTbDialogErrorAlert">
            <strong>Error!</strong>
        </div>
        <form class="form-horizontal">

            <div class="control-group">
                <label class="control-label" for="editTbDialogSource">Source: </label>

                <div class="controls">
                    <input class="span3" type="text" id="editTbDialogSource" value="Source placeholder"
                           disabled="true">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="editTbDialogSourceLocale">Source locale: </label>

                <div class="controls">
                    <input class="span3" type="text" id="editTbDialogSourceLocale" value="Source locale placeholder"
                           disabled="true">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="editTbDialogTargetLocale">Target locale: </label>

                <div class="controls">
                    <input class="span3" type="text" id="editTbDialogTargetLocale" value="Target locale placeholder"
                           disabled="true">
                </div>
            </div>


            <div class="control-group" id="editTbDialogTranslationTextareaControl">
                <label class="control-label" for="editTbDialogTranslationTextarea">Translation: </label>

                <div class="controls">
                    <textarea class="span3" style="height: 200px" id="editTbDialogTranslationTextarea"
                              placeholder="Translation"></textarea>
                </div>
            </div>
            <div class="control-group" id="editTbDialogTranslationInputControl">
                <label class="control-label" for="editTbDialogTranslationInput">Translation: </label>

                <div class="controls">
                    <input class="span3" id="editTbDialogTranslationInput"
                           placeholder="Translation"/>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-success" id="editTbDialogButtonCommit">Commit changes</a>
        <a href="#" class="btn" data-dismiss="modal">Cancel</a>
    </div>
    <script>
        function showEditTbDialogError(message) {
            $('#editTbDialogErrorAlert').show('slow');
            $('#editTbDialogErrorAlert').html('<strong>Error: </strong> ' + message);
        }

        $('#editTbDialogButtonCommit').live('click', function (e) {
            $('#validationErrorAlert').hide();

            var translationInput = $('#editTbDialogTranslationInput').val();
            var translationTextarea = $('#editTbDialogTranslationTextarea').val();
            if (translationInput == 0 && translationTextarea == 0) {
                showEditTbDialogError('Translation field is empty');
                return;
            }

            var changeRequest = {'source': null, 'sourceLocaleCode': null, 'translation': null, 'locale': null};
            changeRequest.source = $('#editTbDialogSource').val();
            changeRequest.sourceLocaleCode = $('#editTbDialogSourceLocale').val();
            changeRequest.locale = $('#editTbDialogTargetLocale').val();
            if (translationTextarea == 0) {
                changeRequest.translation = translationInput;
            } else {
                changeRequest.translation = translationTextarea;
            }
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: 'api/gs/tb',
                data: JSON.stringify(changeRequest),
                dataType: "json",
                success: function (data) {
                    $('#editTbDialogDialog').modal('hide');
                    $('#editTbDialogDialogSuccess').modal('show');
                },
                error: function (error) {
                    showError(error.status + ' ' + error.message);
                }
            });
        });

    </script>
</div>

<div id="editTbDialogDialogSuccess" class="modal hide fade in" style="display: none; ">
    <div class="modal-body">
        <h4>Your change request has been received</h4>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-success" data-dismiss="modal">Dismiss</a>
    </div>
</div>
</body>
</html>