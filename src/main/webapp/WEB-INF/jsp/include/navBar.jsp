<%-- @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt; --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
</head>
<body>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand"><img src="img/logo_globalme.png"></a>
            <ul class="nav pull-right">
                <c:choose>
                    <c:when test="${param.active=='tm'}">
                        <li class="active">
                            <a href="tm">Translation Memory Search</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="tm">Translation Memory Search</a>
                        </li>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${param.active=='tb'}">
                        <li class="active">
                            <a href="tb">Term-Base Search</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="tb">Term-Base Search</a>
                        </li>
                    </c:otherwise>
                </c:choose>
                   <li><a href="<c:url value="/j_spring_security_logout" />">Logout</a></li>


            </ul>
        </div>
    </div>
</div>
</body>
</html>