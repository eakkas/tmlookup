<%-- @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt; --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<img src="img/button-feedback.png" data-toggle="modal" href="#feedbackDialogDialog" class="button-feedback"/>


<div id="feedbackDialogDialog" class="modal hide fade in" style="display: none; ">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>

        <h3>Bug Report</h3>
    </div>
    <div class="modal-body">
        <div class="alert alert-error hide" id="feedbackDialogErrorAlert">
            <strong>Error!</strong>
        </div>
        <form class="form-horizontal">

            <div class="control-group">
                <label class="control-label" for="feedbackDialogName">Name (Optional) </label>

                <div class="controls">
                    <input class="span3" type="text" id="feedbackDialogName" placeholder="Name">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="feedbackDialogEmail">E-mail (Optional) </label>

                <div class="controls">
                    <input class="span3" type="text" id="feedbackDialogEmail" placeholder="E-mail">
                </div>
            </div>


            <div class="control-group">
                <label class="control-label" for="feedbackDialogMessage">Message </label>

                <div class="controls">
                    <textarea class="span3" style="height: 200px" id="feedbackDialogMessage"
                              placeholder="Message"></textarea>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-success" id="feedbackDialogSendButton">Send</a>
        <a href="#" class="btn" data-dismiss="modal">Close</a>
    </div>
    <script>
        function showFeedbackDialogError(message) {
            $('#feedbackDialogErrorAlert').show('slow');
            $('#feedbackDialogErrorAlert').html('<strong>Error: </strong> ' + message);
        }

        $('#feedbackDialogSendButton').live('click', function (e) {
            $('#validationErrorAlert').hide();

            var message = $('#feedbackDialogMessage').val();
            if (message == 0) {
                showFeedbackDialogError('Message field is empty');
                return;
            }
            var report = {'name': null, 'email': null, 'message': null};
            report.name = $('#feedbackDialogName').val();
            report.email = $('#feedbackDialogEmail').val();
            report.message = message;
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: 'api/report',
                data: JSON.stringify(report),
                dataType: "json",
                success: function (data) {
                    $('#feedbackDialogDialog').modal('hide');
                    $('#feedbackDialogDialogSuccess').modal('show');
                },
                error: function (error) {
                    showError(error.status + ' ' + error.message);
                }
            });
        });

    </script>
</div>

<div id="feedbackDialogDialogSuccess" class="modal hide fade in" style="display: none; ">
    <div class="modal-body">
        <h4>Thanks for your feedback!</h4>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-success" data-dismiss="modal">No problem</a>
    </div>
</div>
</body>
</html>