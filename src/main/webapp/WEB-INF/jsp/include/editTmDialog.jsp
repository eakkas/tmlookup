<%-- @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt; --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>

<div id="editTmDialogDialog" class="modal hide fade in" style="display: none; ">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>

        <h3>Edit TM Entry</h3>
    </div>
    <div class="modal-body">
        <div class="alert alert-error hide" id="editTmDialogErrorAlert">
            <strong>Error!</strong>
        </div>
        <form class="form-horizontal">

            <div class="control-group">
                <label class="control-label" for="editTmDialogSource">Source: </label>

                <div class="controls">
                    <input class="span3" type="text" id="editTmDialogSource" value="Source placeholder"
                           disabled="true">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="editTmDialogSourceLocale">Source locale: </label>

                <div class="controls">
                    <input class="span3" type="text" id="editTmDialogSourceLocale" value="Source locale placeholder"
                           disabled="true">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="editTmDialogTargetLocale">Target locale: </label>

                <div class="controls">
                    <input class="span3" type="text" id="editTmDialogTargetLocale" value="Target locale placeholder"
                           disabled="true">
                </div>
            </div>


            <div class="control-group" id="editTmDialogTranslationTextareaControl">
                <label class="control-label" for="editTmDialogTranslationTextarea">Translation: </label>

                <div class="controls">
                    <textarea class="span3" style="height: 200px" id="editTmDialogTranslationTextarea"
                              placeholder="Translation"></textarea>
                </div>
            </div>
            <div class="control-group" id="editTmDialogTranslationInputControl">
                <label class="control-label" for="editTmDialogTranslationInput">Translation: </label>

                <div class="controls">
                    <input class="span3" id="editTmDialogTranslationInput"
                           placeholder="Translation"/>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-success" id="editTmDialogButtonCommit">Commit changes</a>
        <a href="#" class="btn" data-dismiss="modal">Cancel</a>
    </div>
    <script>
        function showEditTmDialogError(message) {
            $('#editTmDialogErrorAlert').show('slow');
            $('#editTmDialogErrorAlert').html('<strong>Error: </strong> ' + message);
        }

        $('#editTmDialogButtonCommit').live('click', function (e) {
            $('#validationErrorAlert').hide();

            var translationInput = $('#editTmDialogTranslationInput').val();
            var translationTextarea = $('#editTmDialogTranslationTextarea').val();
            if (translationInput == 0 && translationTextarea == 0) {
                showEditTmDialogError('Translation field is empty');
                return;
            }

            var changeRequest = {'source': null, 'sourceLocale': null, 'translation': null, 'locale': null};
            changeRequest.source = $('#editTmDialogSource').val();
            changeRequest.sourceLocale = $('#editTmDialogSourceLocale').val();
            changeRequest.locale = $('#editTmDialogTargetLocale').val();
            if (translationTextarea == 0) {
                changeRequest.translation = translationInput;
            } else {
                changeRequest.translation = translationTextarea;
            }
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: 'api/gs/tm',
                data: JSON.stringify(changeRequest),
                dataType: "json",
                success: function (data) {
                    $('#editTmDialogDialog').modal('hide');
                    $('#editTmDialogDialogSuccess').modal('show');
                },
                error: function (error) {
                    showError(error.status + ' ' + error.message);
                }
            });
        });

    </script>
</div>

<div id="editTmDialogDialogSuccess" class="modal hide fade in" style="display: none; ">
    <div class="modal-body">
        <h4>Your change request has been received</h4>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-success" data-dismiss="modal">Dismiss</a>
    </div>
</div>
</body>
</html>