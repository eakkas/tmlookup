<!DOCTYPE HTML>
<%-- @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt; --%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8" />
<title>Globalme TM Lookup Tool</title>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/css/bootstrap-responsive.min.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<script
	src="${pageContext.request.contextPath}/js/libs/jquery/jquery-1.8.3.min.js"></script>
<script
	src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/json2.js"></script>
</head>

<script>
function decodeBase64(e){if(e==undefined){return e}var t={},n,r=0,i,s,o=0,u,a="",f=String.fromCharCode,l=e.length;var c="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";for(n=0;n<64;n++){t[c.charAt(n)]=n}for(s=0;s<l;s++){i=t[e.charAt(s)];r=(r<<6)+i;o+=6;while(o>=8){((u=r>>>(o-=8)&255)||s<l-2)&&(a+=f(u))}}return a}function getUrlParameter(e){var t=window.location.search.substring(1);var n=t.split("&");for(var r=0;r<n.length;r++){var i=n[r].split("=");if(i[0]==e){return i[1]}}}$(document).ready(function(){var e=decodeBase64(getUrlParameter("u"));var t=decodeBase64(getUrlParameter("p"));if(e==undefined||t==undefined){return}$("#j_username").attr("value",e);$("#j_password").attr("value",t);$("#loginForm").submit(function(){var e="${pageContext.request.contextPath}/j_spring_security_check";$.ajax({type:"POST",url:e,data:$("#loginForm").serialize(),success:function(e){if(e.indexOf("Wrong login or password")!=-1){alert("Invalid username/password. Please contact administrator");window.location="${pageContext.request.contextPath}/login"}else{window.location="${pageContext.request.contextPath}"}console.log(e)},error:function(e){console.log(e)}});return false});var n=jQuery.Event("submit");$("#loginForm").trigger(n);if(n.isDefaultPrevented()){}})
</script>


<body>
	<jsp:include page="include/emptyNavBar.jsp" flush="true" />
	<div class="container">
		<div class="content">

			<div class="page-header">
				<h1>Log in</h1>
			</div>

			<div class="row">
				<div class="span12">

					<div class="row">
						<div class="span14">
							<form action="j_spring_security_check" class="form-horizontal"
								method="POST" onsubmit="return validateForm()" id="loginForm">
								<div class="control-group">
									<label class="control-label" for="j_username">Login</label>

									<div class="controls">
										<input type="text" name="j_username" id="j_username"
											placeholder="Login">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="j_password">Password</label>

									<div class="controls">
										<input type="password" name="j_password" id="j_password"
											placeholder="Password">
									</div>
								</div>
								<div class="control-group">
									<div class="controls">
										<input type="submit" class="btn" value="Sign in" />
										<a href="#" onclick="showRequestAccess()">Request Access</a>
									</div>
								</div>
							</form>
						</div>
					</div>
					<c:choose>
                    						<c:when test="${param.errorMessage=='bad.credentials'}">
                    							<div class="alert alert-error" id="validationErrorAlert">
                    								<strong>Error!</strong> Username/password incorrect or user does not exist. Please try again or request a user account using this form
                                                </div>
                                                <div class="span14" id="requestForm">
                    						</c:when>
                    						<c:when test="${param.errorMessage=='bad.role'}">
                                            	<div class="alert alert-error" id="validationErrorAlert">
                                            		<strong>Error!</strong> Your account does not have the necessary privileges. You can request privileges using this form
                                                </div>
                                                <div class="span14" id="requestForm">
                                            </c:when>
                    						<c:otherwise>
                    							<div class="alert alert-error hide" id="validationErrorAlert">
                    								<strong>Error!</strong>
                    							</div>
                    							<div class="span14 hide" id="requestForm">
                    						</c:otherwise>

                    					</c:choose>


                                                                                                                                             		<input type="hidden" name="username" id="username" value="${param.username}"/>
                                                                                                                                             		<div class="control-group">
                                                                                                                                                          <label class="control-label" for="name">Name</label>

                                                                                                                                                              <div class="controls">
                                                                                                                                                                  <input type="text" name="name" id="name" placeholder="Name">
                                                                                                                                                              </div>
                                                                                                                                                     </div>
                                                                                                                                             		<div class="control-group">
                                                                                                                                             			<label class="control-label" for="email">Email</label>

                                                                                                                                             			<div class="controls">
                                                                                                                                             				<input type="text" name="email" id="email"
                                                                                                                                             											placeholder="Valid Email">
                                                                                                                                             			</div>
                                                                                                                                             		</div>
                                                                                                                                             		<div class="control-group">
                                                                                                                                             			<label class="control-label" for="description">Why do you need access?</label>

                                                                                                                                             		    <div class="controls">
                                                                                                                                                            <textarea rows="10" cols="45" maxlength="500" name="description" id="description"></textarea>
                                                                                                                                                        </div>
                                                                                                                                             		</div>
                                                                                                                                             		<div class="control-group">
                                                                                                                                             			<div class="controls">
                                                                                                                                             			    <input type="button" onclick="validateAndRequestForm()" value="Request access">

                                                                                                                                             			</div>
                                                                                                                                             		</div>

                                                                                        </div>
                    					<div class="alert hide" id="status">
                                             <strong>Your request has been sent</strong>
                                        </div>
				</div>
			</div>

		</div>
		<%@ include file="include/footer.jsp"%>
	</div>
	<script type="text/javascript" charset="utf-8">
		function showError(message) {
			$('#validationErrorAlert').addClass('show');
			$('#validationErrorAlert').html(
					'<strong>Error: </strong> ' + message);
		}
		function showRequestAccess(){
		    $('#requestForm').show();
		}
		function validateForm() {
			if ($('#j_username').val().length == 0) {
				showError('Login field is empty');
				return false;
			}
			if ($('#j_password').val().length == 0) {
				showError('Password field is empty');
				return false;
			}
		}
		function validateAndRequestForm() {
        			if ($('#name').val().length == 0) {
        				showError('Name field is empty');
        				return false;
        			}
        			if ($('#email').val().length == 0) {
        				showError('Email field is empty');
        				return false;
        			}
        			if ($('#description').val().length == 0) {
                        showError('Request text field is empty');
                        return false;
                    }
                    $.ajax({
                                        type: 'GET',
                                        contentType: 'application/json; charset=utf-8',
                                        url: 'ra?name=' + $('#name').val() + '&email=' + $('#email').val() + '&description=' + $('#description').val() + '&username=' + $('#username').val(),
                                        success: function (data) {
                                            $('#buttonRequest').button('reset');
                                            $('#status').show();
                                            $('#validationErrorAlert').hide();
                                            $('#requestForm').hide();
                                        },
                                        error: function (error) {
                                            $('#buttonRequest').button('reset');
                                            showError(error.status + '');
                                        }
                                    });
        }


	</script>
</body>

</html>