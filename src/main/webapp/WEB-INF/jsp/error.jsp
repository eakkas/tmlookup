<!DOCTYPE HTML>
<%-- @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt; --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>agileTranslate | Error</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/js/libs/jquery/jquery-1.8.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/main.js"></script>
    <script src="${pageContext.request.contextPath}/js/json2.js"></script>
</head>
<body>
<%@ include file="include/feedbackDialog.jsp" %>
<jsp:include page="include/emptyNavBar.jsp" flush="true"/>
<div class="container">
    <div class="content">

        <div class="page-header">
            <h1>
                <c:choose>
                    <c:when test="${param.error=='404'}">
                        404
                    </c:when>
                    <c:when test="${param.error=='403'}">
                        403
                    </c:when>
                    <c:when test="${param.error=='500'}">
                        500
                    </c:when>
                    <c:when test="${param.error=='503'}">
                        503
                    </c:when>
                    <c:otherwise>
                        Error
                    </c:otherwise>
                </c:choose>
            </h1>

        </div>

        <div class="row">
            <div class="span12">
                <h2>
                    <c:choose>
                        <c:when test="${param.error=='404'}">
                            Web page not found
                        </c:when>
                        <c:when test="${param.error=='403'}">
                            Access denied
                        </c:when>
                        <c:when test="${param.error=='500'}">
                            Internal error
                        </c:when>
                        <c:when test="${param.error=='503'}">
                            Service unavailable
                        </c:when>
                        <c:when test="${message!=null}">
                            ${message}
                        </c:when>

                        <c:otherwise>
                            Some error occurred.
                        </c:otherwise>
                    </c:choose>
                </h2>
                <a href="${pageContext.request.contextPath}/">Go to the home page</a>
            </div>
        </div>

    </div>
    <%@ include file="include/footer.jsp" %>
</div>
</body>
</html>