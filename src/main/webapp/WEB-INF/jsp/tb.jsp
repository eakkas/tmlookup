<%@ page import="net.globalme.gstestingtool.machinetranslation.entity.Locale" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<!DOCTYPE html>
<%-- @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt; --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Globalme TM Lookup Tool</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/js/libs/jquery/jquery-1.8.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/libs/bootstrap/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/main.js"></script>
    <script src="${pageContext.request.contextPath}/js/json2.js"></script>
</head>
<body>
<%-- <%@ include file="include/feedbackDialog.jsp" %>
 --%><jsp:include page="include/navBar.jsp" flush="true">
    <jsp:param name="active" value="tb"/>
    <jsp:param name="name" value="${name}"/>
</jsp:include>
<div class="container">
    <div class="content">
        <div class="page-header">
            <h1>Term-Base Search</h1>
        </div>


        <div class="row">
            <div class="span12">
                <div class="alert alert-error hide" id="validationErrorAlert">
                    <strong>Error!</strong>
                </div>
                <form class="form" action="javascript:void(0);" autocomplete="off">
                    <div class="controls controls-row">
                        <label class="span6">Text to search</label>
                        <label class="span2">Source language</label>
                        <label class="span2">Target language</label>
                    </div>
                    <div class="controls controls-row">
                        <input id="textTbSearch" type="text" class="span6"/>
                        <select id="selectTbSource" class="span2">
                            <% final Map<Locale, ArrayList<Locale>> locales = (Map<Locale, ArrayList<Locale>>) request.getAttribute("locales");
                                Locale selectedLocale = null;
                                for (final Locale locale : locales.keySet()) {
                                    if (selectedLocale == null) {
                                        selectedLocale = locale;
                                    }%>
                            <option value="<%=locale.getCode()%>"><%=locale.getDisplayName()%>
                            </option>
                            <%}%>
                        </select>
                        <select id="selectTbTarget" class="span2">
                            <% if (selectedLocale != null) {
                                for (final Locale locale : locales.get(selectedLocale)) {%>
                            <option value="<%=locale.getCode()%>"><%=locale.getDisplayName()%>
                            </option>
                            <%
                                    }
                                }
                            %>
                        </select>

                        <div class="span1">
                            <button id="buttonTbSearch" class="btn btn-primary button-search">Search</button>
                        </div>
                    </div>
                </form>
                </br>
                <div id="results" class="hide">
                    <table class="table table-striped table-hover" id="tableTbResults">
                        <h3>Results</h3>
                        <thead>
                        <tr>
                            <th class="span7">Source</th>
                            <th class="span7">Translation</th>
                            <th class="span7">Part of speech</th>
                            <th class="span7">Domain</th>
                            <th class="span7">Definition</th>
                            <th class="span7">Context</th>
                            <th class="span2">Locale</th>
                        </tr>
                        </thead>
                        <tbody id="tableBodyTbResults">
                        </tbody>
                    </table>
                </div>
                <div id="noResults" class="hide">
                    <h3>No matches found</h3>
                </div>

                <div id="additionalResults" class="hide">
                    <table class="table table-striped table-hover" id="tableAdditionalTbResults">
                        <tbody id="tableBodyAdditionalTbResults">
                        </tbody>
                    </table>
                </div>
                <div class="pull-right showAdditionalResultsButtonContainer hide"
                     id="showAdditionalResultsButtonContainer">
                    <a href="#"
                       onclick="$('#showAdditionalResultsButtonContainer').hide();$('#additionalResults').show('slow');">
                        + Show matches for all languages</a>
                </div>
            </div>
        </div>
    </div>
    <%@ include file="include/footer.jsp" %>
</div>
<script type="text/javascript" charset="utf-8">
    function showError(message) {
        var elementValidationError = $('#validationErrorAlert');
        elementValidationError.addClass('show');
        elementValidationError.html('<strong>Error: </strong> ' + message);
    }

    function hideAllResults() {
        $('#noResults').hide();
        $('#results').hide();
        $('#additionalResults').hide();
        $('#showAdditionalResultsButtonContainer').hide();
    }

    function fillTablesWithResults(data) {
        var elementTableBodyResults = $('#tableBodyTbResults');
        var elementTableBodyAdditionalResults = $('#tableBodyAdditionalTbResults');

        elementTableBodyResults.html('');
        elementTableBodyAdditionalResults.html('');

        var targetMatchesNumber = 0;

        var targetLanguage = $('#selectTbTarget').find('option:selected').text();

        for (var i = 0; i < data.length; i++) {
            if (targetLanguage.indexOf(data[i].locale) != -1) {
                elementTableBodyResults.append('<tr><td>' + data[i].source + '</td>'+
                        '<td>' + data[i].translation + '</td>' +
                        '<td>' + data[i].partOfSpeech + '</td>'+
                        '<td>' + data[i].domain + '</td>'+
                        '<td>' + data[i].definition + '</td>'+
                        '<td>' + data[i].context + '</td>'+
                        '<td>' + data[i].locale + '</td></tr>');
            } else {
                elementTableBodyAdditionalResults.append('<tr><td class="span7">' + data[i].source +
                        '</td><td class="span7">' + data[i].translation + '</td>' +
                        '</td><td class="span7">' + data[i].partOfSpeech + '</td>' +
                        '</td><td class="span7">' + data[i].domain + '</td>' +
                        '</td><td class="span7">' + data[i].definition + '</td>' +
                        '</td><td class="span7">' + data[i].context + '</td>' +
                        '<td class="span2">' + data[i].locale + '</td>' +
                        '</tr>');
            }
        }

        if (targetMatchesNumber != data.length) {
            $('#showAdditionalResultsButtonContainer').show();
        } else {
            $('#showAdditionalResultsButtonContainer').hide();
        }
    }

    function onSearchSuccess(data) {
        hideAllResults();

        var elementResults = $('#results');
        var elementNoResults = $('#noResults');
        if (data.length == 0) {
            elementResults.hide();
            elementNoResults.show('slow');
            return;
        }

        fillTablesWithResults(data);

        elementNoResults.hide();
        elementResults.show('slow');
    }

    var locales = {};
    <%for (final Locale sourceLocale : locales.keySet()) {%>
    locales['<%=sourceLocale.getCode()%>'] = [];
    <%for(final Locale targetLocale : locales.get(sourceLocale)){%>
    locales['<%=sourceLocale.getCode()%>'][<%=locales.get(sourceLocale).indexOf(targetLocale)%>] =
    {'code': '<%=targetLocale.getCode()%>', 'displayName': '<%=targetLocale.getDisplayName()%>'};
    <%}
    }%>

    var elementSelectTbSource = $('#selectTbSource');
    elementSelectTbSource.change(function () {
        var elementSelectTbTarget = $('#selectTbTarget');
        elementSelectTbTarget.html('');
        for (var i = 0; i < locales[elementSelectTbSource.val()].length; i++) {
            elementSelectTbTarget.append('<option value="' + locales[elementSelectTbSource.val()][i].code +
                    '">' + locales[elementSelectTbSource.val()][i].displayName + '</option>');
        }
    });

    $('#buttonTbSearch').live('click', function (e) {
        $('#validationErrorAlert').hide();

        var query = $('#textTbSearch').val();
        if (query == 0) {
            showError('Search query is empty');
            return;
        }

        $('#buttonTbSearch').button('loading');

        var sourceLanguage = $('#selectTbSource').val();
        var targetLanguage = $('#selectTbTarget').find('option:selected').text();
        var targetLocaleCode = $('#selectTbTarget').val();

        if (sourceLanguage == targetLanguage) {
            showError('Source language is equal to target language');
            return;
        }

        $.ajax({
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            url: 'api/gs/tb?q=' + query + '&from=' + sourceLanguage + '&to=' + targetLocaleCode,
            success: function (data) {
                $('#buttonTbSearch').button('reset');
                onSearchSuccess(data)
            },
            error: function (error) {
                $('#buttonTbSearch').button('reset');
                showError(error.status + '');
            }
        });
    });
</script>
</body>
</html>