package net.globalme.gstestingtool;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public interface Config {
	public final static String APP_NAME = "cbn";
	public static final int ACCESS_TOKEN_LENGTH = 32;
	public static int TOKEN_EXPIRY_TIME = 15 * 60 * 1000; // 15min. in ms
}
