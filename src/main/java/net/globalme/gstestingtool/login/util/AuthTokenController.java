package net.globalme.gstestingtool.login.util;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import net.globalme.gstestingtool.service.exception.InternalErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("auth/token")
public class AuthTokenController {

	@Autowired
	@Qualifier("authenticationManager")
	protected AuthenticationManager authenticationManager;

	@RequestMapping(method = GET, value = "generate")
	@ResponseBody
	String generateAuthToken(@RequestParam("callback") String callback)
			throws InternalErrorException {
		AuthToken token = AuthTokenUtil.generateAuthToken();
		return callback + "({'token': '" + token.getToken() + "'});";
	}

	// @RequestMapping(method = GET, value = "validate")
	// @ResponseBody
	// boolean validateAuthToken(@RequestParam("token") String token) {
	// boolean isValid = AuthTokenUtil.isValidToken(token);
	// return isValid;
	// }

}
