package net.globalme.gstestingtool.login.util;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.crypto.codec.Base64;

/**
 * Servlet implementation class AuthServlet
 */
public class AuthServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AuthServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String token = request.getParameter("token");

		if (AuthTokenUtil.isValidToken(token)) {
			String user = new String(Base64.encode("user".getBytes()));
			String password = new String(Base64.encode("p455w0Rd".getBytes()));
			response.sendRedirect(request.getContextPath() + "/login?u=" + user
					+ "&p=" + password);
		} else {
			response.getWriter().write("false");
		}
	}

}
