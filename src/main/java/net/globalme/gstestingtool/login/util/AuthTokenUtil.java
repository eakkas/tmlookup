package net.globalme.gstestingtool.login.util;

import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import net.globalme.gstestingtool.Config;
import net.globalme.gstestingtool.service.exception.InternalErrorException;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

public class AuthTokenUtil {

	private static final Map<String, AuthToken> authTokens = new TreeMap<String, AuthToken>();

	private static AuthToken newToken() {
		AuthToken token = new AuthToken();

		String randomToken = RandomStringUtils
				.randomAlphanumeric(Config.ACCESS_TOKEN_LENGTH);
		token.setToken(randomToken);
		token.setGeneratedTime(System.currentTimeMillis());
		return token;
	}

	// public static String generateAuthToken() throws InternalErrorException {
	// int tries = 0;
	// // for safety, we just try 10 times to generate random tokens.
	// while (tries++ < 10) {
	// AuthToken token = newToken();
	// if (!authTokens.containsKey(token.getToken())) {
	// authTokens.put(token.getToken(), token);
	// return token.getToken();
	// }
	// }
	//
	// throw new InternalErrorException(
	// "Could not generate Random Number. Please try again.");
	// }

	public static AuthToken generateAuthToken() throws InternalErrorException {
		int tries = 0;
		// for safety, we just try 10 times to generate random tokens.
		while (tries++ < 10) {
			AuthToken token = newToken();
			if (!authTokens.containsKey(token.getToken())) {
				authTokens.put(token.getToken(), token);
				return token;
			}
		}

		throw new InternalErrorException(
				"Could not generate Random Number. Please try again.");
	}

	public static boolean isValidToken(String authToken) {
		if (StringUtils.isBlank(authToken)) {
			return false;
		}
		AuthToken token = authTokens.get(authToken);
		if (token == null) {
			return false;
		}

		if (System.currentTimeMillis() - token.getGeneratedTime() > Config.TOKEN_EXPIRY_TIME) {
			return false;
		}

		return true;
	}

	public static void login(HttpServletRequest request) {

		
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
				"user", null, AuthorityUtils.createAuthorityList("ROLE_ADMIN"));
		// auth.setAuthenticated(true);
		auth.setDetails(new WebAuthenticationDetails(request));
		SecurityContextHolder.getContext().setAuthentication(auth);
	}

}
