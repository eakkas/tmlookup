package net.globalme.gstestingtool.login.util;

public class AuthToken {

	private String token;
	private long generatedTime;

	public AuthToken() {
	}

	public AuthToken(String authToken) {
		this.token = authToken;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getGeneratedTime() {
		return generatedTime;
	}

	public void setGeneratedTime(long generatedTime) {
		this.generatedTime = generatedTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthToken other = (AuthToken) obj;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}

}
