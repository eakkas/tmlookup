package net.globalme.gstestingtool.service;

import net.globalme.gstestingtool.entity.Term;
import net.globalme.gstestingtool.entity.TranslationMemory;
import net.globalme.gstestingtool.machinetranslation.entity.Locale;
import net.globalme.gstestingtool.service.exception.BadArgumentException;
import net.globalme.gstestingtool.service.exception.ForbiddenException;
import net.globalme.gstestingtool.service.exception.InternalErrorException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public interface GSService {
    public Map<Locale, ArrayList<Locale>> getAvailableLocales();

    public void setAvailableLocales(Map<Locale, ArrayList<Locale>> availableLocales);

    public List<Term> getTermTranslations(final String sourceString, final String sourceLanguage) throws
            BadArgumentException, InternalErrorException;

    public ArrayList<TranslationMemory> getTranslationMemoryMatches(final String sourceString,
                                                                    final String sourceLanguage) throws
            BadArgumentException, InternalErrorException;

    public boolean isInitialized();

    public String getInitializingError();

    public void setInitializingError(String initializingError);

    void editTmEntry(final TranslationMemory translationMemory) throws BadArgumentException, InternalErrorException;

    void editTbEntry(final Term term) throws BadArgumentException, InternalErrorException;

    void deleteTmEntry(final String source, final String sourceLocale, final String targetLocale) throws
            BadArgumentException, InternalErrorException, ForbiddenException;

    void deleteTbEntry(final String source, final String sourceLocale, final String targetLocale) throws
            BadArgumentException, InternalErrorException, ForbiddenException;

    void requestAccess(String name, String email, String description, String username);
}
