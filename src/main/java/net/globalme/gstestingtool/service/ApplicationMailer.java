package net.globalme.gstestingtool.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * Created by nerseszakoyan on 16.05.16.
 */

public class ApplicationMailer
{
    private MailSender mailSender;

    private SimpleMailMessage preConfiguredMessage;

    private final static Logger log = Logger.getLogger(ApplicationMailer.class);

    public void setPreConfiguredMessage(SimpleMailMessage preConfiguredMessage) {
        this.preConfiguredMessage = preConfiguredMessage;
    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }


    /**
     * This method will send compose and send the message
     * */
    public void sendMail(String to, String subject, String body)
    {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);
        mailSender.send(message);
    }

    /**
     * This method will send a pre-configured message
     * */
    public void sendPreConfiguredMail(String name, String email, String description, String username)
    {


        SimpleMailMessage mailMessage = new SimpleMailMessage(preConfiguredMessage);
        mailMessage.setText(String.format(preConfiguredMessage.getText(), username, name, email, description));
        mailSender.send(mailMessage);

        log.info(mailMessage.toString());
    }
}