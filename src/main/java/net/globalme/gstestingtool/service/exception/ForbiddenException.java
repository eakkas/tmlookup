package net.globalme.gstestingtool.service.exception;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class ForbiddenException extends Exception {

    public ForbiddenException(final String message) {
        super(message);
    }
}
