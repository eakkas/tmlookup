package net.globalme.gstestingtool.service.exception;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class NoAccessException extends Exception {

    public NoAccessException() {
        super("You don't have rights to perform this action");
    }

    public NoAccessException(final String message) {
        super(message);
    }
}
