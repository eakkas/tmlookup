package net.globalme.gstestingtool.service.exception;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class BadArgumentException extends Exception {
    public BadArgumentException(final String message) {
        super(message);
    }
}
