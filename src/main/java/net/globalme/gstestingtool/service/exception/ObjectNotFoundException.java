package net.globalme.gstestingtool.service.exception;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class ObjectNotFoundException extends Exception {
    public ObjectNotFoundException(final String message) {
        super(message);
    }
}
