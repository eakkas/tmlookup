package net.globalme.gstestingtool.service.exception;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class InternalErrorException extends Exception {

	public InternalErrorException() {
		super();
	}

	public InternalErrorException(String string) {
		super(string);
	}
}
