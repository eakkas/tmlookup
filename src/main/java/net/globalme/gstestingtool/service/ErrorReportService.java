package net.globalme.gstestingtool.service;

import net.globalme.gstestingtool.entity.ErrorReport;
import net.globalme.gstestingtool.service.exception.BadArgumentException;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public interface ErrorReportService {
    public void reportError(final ErrorReport errorReport) throws BadArgumentException;
}
