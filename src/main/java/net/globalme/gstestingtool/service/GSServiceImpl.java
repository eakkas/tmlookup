package net.globalme.gstestingtool.service;

import net.globalme.gstestingtool.entity.GSConfiguration;
import net.globalme.gstestingtool.entity.Term;
import net.globalme.gstestingtool.entity.TranslationMemory;
import net.globalme.gstestingtool.machinetranslation.ConfigurationProvider;
import net.globalme.gstestingtool.machinetranslation.GlobalSightClient;
import net.globalme.gstestingtool.machinetranslation.entity.Locale;
import net.globalme.gstestingtool.machinetranslation.entity.LocalePair;
import net.globalme.gstestingtool.machinetranslation.exception.ConfigurationException;
import net.globalme.gstestingtool.persistence.ChangeRequestDao;
import net.globalme.gstestingtool.service.exception.BadArgumentException;
import net.globalme.gstestingtool.service.exception.ForbiddenException;
import net.globalme.gstestingtool.service.exception.InternalErrorException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.util.*;

import static com.google.common.base.Strings.isNullOrEmpty;
import static net.globalme.gstestingtool.util.LocaleUtil.renameLanguageIfNeeded;
import static org.apache.log4j.Logger.getLogger;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@Service
public class GSServiceImpl implements GSService {

    private final static Logger log = getLogger(GSServiceImpl.class);

    @Inject
    private GlobalSightClient globalSightClient;

    @Inject
    private ConfigurationProvider configurationProvider;

    @Inject
    private ChangeRequestDao changeRequestDao;

    private Map<Locale, ArrayList<Locale>> availableLocales;

    private String initializingError;

    private GSConfiguration configuration;

    public Map<Locale, ArrayList<Locale>> getAvailableLocales() {
        return availableLocales;
    }

    public void setAvailableLocales(Map<Locale, ArrayList<Locale>> availableLocales) {
        this.availableLocales = availableLocales;
    }

    public List<Term> getTermTranslations(String sourceString, String sourceLanguage) throws BadArgumentException,
            InternalErrorException {
        if (isNullOrEmpty(sourceString)) {
            throw new BadArgumentException("sourceString is null or empty");
        }
        if (isNullOrEmpty(sourceLanguage)) {
            throw new BadArgumentException("sourceLanguage is null opr empty");
        }
        if (isNullOrEmpty(sourceLanguage)) {
            throw new BadArgumentException("targetLanguage is null opr empty");
        }
        try {
            return globalSightClient.getTermTranslations(sourceString, sourceLanguage);
        } catch (IOException e) {
            log.error("IOException while getting term translations", e);
            throw new InternalErrorException();
        }
    }

    public ArrayList<TranslationMemory> getTranslationMemoryMatches(String sourceString, String sourceLanguage) throws BadArgumentException, InternalErrorException {
        if (isNullOrEmpty(sourceString)) {
            throw new BadArgumentException("sourceString is null or empty");
        }
        if (isNullOrEmpty(sourceLanguage)) {
            throw new BadArgumentException("sourceLanguage is null opr empty");
        }
        try {
            return globalSightClient.getTranslationMemoryMatches(sourceString, sourceLanguage);
        } catch (IOException e) {
            log.error("IOException while getting term translations", e);
            throw new InternalErrorException();
        }
    }

    public boolean isInitialized() {
        return initializingError == null;// && globalSightClient.isConfigured() && availableLocales != null;
    }

    public String getInitializingError() {
        return initializingError;
    }

    public void editTmEntry(final TranslationMemory translationMemory) throws BadArgumentException, InternalErrorException {
        if (isNullOrEmpty(translationMemory.getSource())) {
            throw new BadArgumentException("translationMemory.source is null or empty");
        }
        if (isNullOrEmpty(translationMemory.getSourceLocale())) {
            throw new BadArgumentException("translationMemory.sourceLocale is null or empty");
        }
        if (isNullOrEmpty(translationMemory.getTranslation())) {
            throw new BadArgumentException("translationMemory.translation is null or empty");
        }
        if (isNullOrEmpty(translationMemory.getLocale())) {
            throw new BadArgumentException("translationMemory.locale is null or empty");
        }
        try {
            changeRequestDao.saveTmChangeRequest(translationMemory);
        } catch (IOException e) {
            log.error("IOException while editing tm entry", e);
            throw new InternalErrorException();
        }
    }

    public void editTbEntry(final Term term) throws BadArgumentException, InternalErrorException {
        if (isNullOrEmpty(term.getSource())) {
            throw new BadArgumentException("term.source is null or empty");
        }
        if (isNullOrEmpty(term.getTranslation())) {
            throw new BadArgumentException("term.translation is null or empty");
        }
        if (isNullOrEmpty(term.getLocale())) {
            throw new BadArgumentException("term.locale is null or empty");
        }
        try {
            changeRequestDao.saveTbChangeRequest(term);
        } catch (IOException e) {
            log.error("IOException while editing tb entry", e);
            throw new InternalErrorException();
        }
    }

    public void deleteTmEntry(final String source, final String sourceLocale, final String targetLocale) throws
            BadArgumentException, InternalErrorException, ForbiddenException {
        if (!configuration.isAllowDeletion()) {
            throw new ForbiddenException("usage of this method is forbidden");
        }
        if (isNullOrEmpty(source)) {
            throw new BadArgumentException("source is null or empty");
        }
        if (isNullOrEmpty(sourceLocale)) {
            throw new BadArgumentException("sourceLocale is null");
        }
        if (isNullOrEmpty(targetLocale)) {
            throw new BadArgumentException("targetLocale is null");
        }
        try {
            globalSightClient.deleteTmEntry(source, sourceLocale, targetLocale);
        } catch (IOException e) {
            log.error("IOException while deleting tm entry", e);
            throw new InternalErrorException();
        }
    }

    public void deleteTbEntry(final String source, final String sourceLocale, final String targetLocale) throws
            BadArgumentException, InternalErrorException, ForbiddenException {
        if (!configuration.isAllowDeletion()) {
            throw new ForbiddenException("usage of this method is forbidden");
        }
        if (isNullOrEmpty(source)) {
            throw new BadArgumentException("source is null or empty");
        }
        if (isNullOrEmpty(sourceLocale)) {
            throw new BadArgumentException("sourceLocale is null");
        }
        if (isNullOrEmpty(targetLocale)) {
            throw new BadArgumentException("targetLocale is null");
        }
        try {
            globalSightClient.deleteTbEntry(source, sourceLocale, targetLocale);
        } catch (IOException e) {
            log.error("IOException while deleting tb entry", e);
            throw new InternalErrorException();
        }
    }

//    private static Map<Locale, ArrayList<Locale>> mergeLocales(final ArrayList<LocalePair> localePairs,
//                                                               final boolean onlyEnglishInSource) {
//        final Map<Locale, ArrayList<Locale>> result = new HashMap<Locale, ArrayList<Locale>>();
//        for (final LocalePair localePair : localePairs) {
//
//            localePair.setSourceLocale(renameLanguageIfNeeded(localePair.getSourceLocale()));
//            localePair.setTargetLocale(renameLanguageIfNeeded(localePair.getTargetLocale()));
//
//            if (onlyEnglishInSource && !localePair.getSourceLocale().getCode().equals("en_US")) {
//                continue;
//            }
//
//            if (!result.containsKey(localePair.getSourceLocale())) {
//                result.put(localePair.getSourceLocale(), new ArrayList<Locale>());
//            }
//            result.get(localePair.getSourceLocale()).add(localePair.getTargetLocale());
//        }
//        return result;
//    }

    @PostConstruct
    private void configure() {
        log.info("starting to configure the app");
        try {
            log.info("configuring gs client");
            configuration = configurationProvider.getConfiguration();
            globalSightClient.configure(configuration);
            log.info("configuring gs client: done");
        } catch (ConfigurationException e) {
            log.error("error while configuring gs client", e);
            initializingError = e.getMessage();
            return;
        }


        log.info("the app was configured successfully");
    }

    public void requestAccess(String name, String email, String description, String username) {
        globalSightClient.requestAccess(name, email, description, username);
    }


    public void setInitializingError(String initializingError) {
        this.initializingError = initializingError;
    }
}
