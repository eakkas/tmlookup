package net.globalme.gstestingtool.service;

import net.globalme.gstestingtool.entity.ErrorReport;
import net.globalme.gstestingtool.service.exception.BadArgumentException;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.google.common.base.Strings.isNullOrEmpty;
import static net.globalme.gstestingtool.Config.APP_NAME;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@Service
public class ErrorReportServiceImpl implements ErrorReportService {
    private final static Logger log = Logger.getLogger(ErrorReportServiceImpl.class);

    private final static String PATH_REPORTS_FOLDER = "/home/ubuntu/" + APP_NAME + "/reports/";
    private final static String FILENAME_REPORTS = "reports.json";

    private File getReportsFile() {
        return new File(getReportsFolder(), FILENAME_REPORTS);
    }

    private File getReportsFolder() {
        final File reportsFolder = new File(PATH_REPORTS_FOLDER);
        if (!reportsFolder.exists()) {
            log.info("creating folder for error reports at " + PATH_REPORTS_FOLDER);
            reportsFolder.mkdirs();
        }
        return reportsFolder;
    }

    @Override
    public void reportError(final ErrorReport errorReport) throws BadArgumentException {
        if (errorReport == null) {
            throw new BadArgumentException("errorReport is null");
        }
        if (isNullOrEmpty(errorReport.getMessage())) {
            throw new BadArgumentException("errorReport.message is null or empty");
        }
        errorReport.setDate(new Date());

        if (getReportsFile().exists()) {
            appendToFile(getReportsFile(), ", \n");
        }
        try {
            final ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setDateFormat(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"));
            appendToFile(getReportsFile(), objectMapper.writeValueAsString(errorReport));
        } catch (IOException e) {
            log.error("IOException while writing ErrorReport as json string");
        }
    }

    private void appendToFile(final File file, final String text) {
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
            out.print(text);
            out.close();
        } catch (IOException e) {
            log.error("IOException while appending text to file: text: " + text, e);
        }
    }
}
