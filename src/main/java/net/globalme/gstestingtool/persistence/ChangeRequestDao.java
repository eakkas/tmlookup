package net.globalme.gstestingtool.persistence;

import net.globalme.gstestingtool.entity.Term;
import net.globalme.gstestingtool.entity.TranslationMemory;

import java.io.IOException;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public interface ChangeRequestDao {
    public void saveTmChangeRequest(final TranslationMemory translationMemory) throws IOException;

    public void saveTbChangeRequest(final Term term) throws IOException;
}
