package net.globalme.gstestingtool.persistence;

import net.globalme.gstestingtool.entity.Term;
import net.globalme.gstestingtool.entity.TranslationMemory;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static net.globalme.gstestingtool.Config.APP_NAME;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@Repository
public class ChangeRequestDaoImpl implements ChangeRequestDao {
    private final static Logger log = Logger.getLogger(ChangeRequestDaoImpl.class);

    private final static String DATE_FORMAT = "yyyyMMdd'T'HHmmss'Z'";
    private final static DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

    private final static String PATH_CHANGE_REQUESTS_FOLDER = "/home/ubuntu/" + APP_NAME + "/changerequests/";
    private final static String FILENAME_CHANGE_REQUESTS = "tm_export_0.tmx";

    private File getRequestsFile() {
        return new File(getRequestsFolder(), FILENAME_CHANGE_REQUESTS);
    }

    private File getRequestsFolder() {
        final File reportsFolder = new File(PATH_CHANGE_REQUESTS_FOLDER);
        if (!reportsFolder.exists()) {
            log.info("creating folder for error reports at " + PATH_CHANGE_REQUESTS_FOLDER);
            reportsFolder.mkdirs();
        }
        return reportsFolder;
    }

    @Override
    public void saveTmChangeRequest(final TranslationMemory translationMemory) throws IOException {
        if (translationMemory == null) {
            throw new IllegalArgumentException("translationMemory is null");
        }
        appendToFile(getRequestsFile(), translationMemory.toChangeRequest(dateFormat.format(new Date()), "gstool", "TMbuilder"));
    }

    @Override
    public void saveTbChangeRequest(final Term term) throws IOException {
        throw new UnsupportedOperationException();
    }

    private void appendToFile(final File file, final String text) throws IOException {
        Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
        out.write(text);
        out.close();
    }
}
