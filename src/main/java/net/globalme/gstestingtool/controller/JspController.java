package net.globalme.gstestingtool.controller;

import net.globalme.gstestingtool.service.GSService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@Controller
public class JspController {
    private final static Logger log = Logger.getLogger(JspController.class);

    @Inject
    GSService gsService;

    @RequestMapping("/")
    public String index() {
        return "home";
    }


    @RequestMapping("/ra")
    @ResponseBody
    public void requestAccess(@RequestParam("name") String name,
                       @RequestParam("email") String email,
                       @RequestParam("description") String description,
                       @RequestParam("username") String username) {

        log.info(name+":"+email +":"+ description+":"+ username);
        gsService.requestAccess(name, email, description, username);

    }
//    @RequestMapping("/ra")
//    public String raPage(ModelMap model) {
//        log.info(model.get("name").toString()+":"+model.get("email").toString() +":"+ model.get("description").toString()+":"+ model.get("username").toString());
//        gsService.requestAccess(model.get("name").toString(), model.get("email").toString(), model.get("description").toString(), model.get("username").toString());
//        return "login";
//    }

    @RequestMapping("/error")
    public String error(ModelMap model) {
        log.info("is initialized: " + gsService.isInitialized());
        if (!gsService.isInitialized()) {
            log.info("setting error message to: " + gsService.getInitializingError());
            model.addAttribute("message", gsService.getInitializingError());
        }
        return "error";
    }

    @RequestMapping("login")
    public String login() {
        return "login";
    }

    @RequestMapping("tm")
    public String tmPage(ModelMap model) {
        model.addAttribute("locales", gsService.getAvailableLocales());
        return "tm";
    }

    @RequestMapping("tb")
    public String tbPage(ModelMap model) {
        model.addAttribute("locales", gsService.getAvailableLocales());
        return "tb";
    }
}
