package net.globalme.gstestingtool.controller.interceptor;

import net.globalme.gstestingtool.service.GSService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@Component
public class ConfigurationErrorInterceptor extends HandlerInterceptorAdapter {

    private final static Logger log = Logger.getLogger(ConfigurationErrorInterceptor.class);

    @Inject
    GSService gsService;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (!gsService.isInitialized() && !request.getRequestURI().endsWith("error")) {
            try {
                response.sendRedirect("error");
            } catch (IOException e) {
                log.error("error while redirecting to configuration error page", e);
            }
            return false;
        } else {
            return true;
        }

    }
}
