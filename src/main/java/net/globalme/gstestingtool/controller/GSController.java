package net.globalme.gstestingtool.controller;

import net.globalme.gstestingtool.controller.error.exception.BadRequestRestException;
import net.globalme.gstestingtool.controller.error.exception.ForbiddenRestException;
import net.globalme.gstestingtool.controller.error.exception.InternalErrorRestException;
import net.globalme.gstestingtool.controller.error.exception.OkRestException;
import net.globalme.gstestingtool.entity.Term;
import net.globalme.gstestingtool.entity.TranslationMemory;
import net.globalme.gstestingtool.service.GSService;
import net.globalme.gstestingtool.service.exception.BadArgumentException;
import net.globalme.gstestingtool.service.exception.ForbiddenException;
import net.globalme.gstestingtool.service.exception.InternalErrorException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@Controller
@RequestMapping("api/gs")
public class GSController {
    @SuppressWarnings("unused")
    private final static Logger log = Logger.getLogger(GSController.class);

    @Inject
    private GSService gsService;

    @RequestMapping(method = GET, value = "tm")
    @ResponseBody
    ArrayList<TranslationMemory> getTmMatches(@RequestParam("q") String query,
                                              @RequestParam("from") String sourceLanguage) {

        try {
            return gsService.getTranslationMemoryMatches(query, sourceLanguage);
        } catch (BadArgumentException e) {
            throw new BadRequestRestException(e.getMessage());
        } catch (InternalErrorException e) {
            throw new InternalErrorRestException();
        }
    }

    @RequestMapping(method = GET, value = "tb")
    @ResponseBody
    List<Term> getTbMatches(@RequestParam("q") String query,
                            @RequestParam("from") String sourceLanguage) {
        try {
            return gsService.getTermTranslations(query, sourceLanguage);
        } catch (BadArgumentException e) {
            throw new BadRequestRestException(e.getMessage());
        } catch (InternalErrorException e) {
            throw new InternalErrorRestException();
        }
    }

    @RequestMapping(method = POST, value = "tm")
    void editTmEntry(@RequestBody TranslationMemory translationMemory) {
        try {
            gsService.editTmEntry(translationMemory);
        } catch (BadArgumentException e) {
            throw new BadRequestRestException(e.getMessage());
        } catch (InternalErrorException e) {
            throw new InternalErrorRestException();
        }
        throw new OkRestException("Translation memory entry was successfully edited");
    }

    @RequestMapping(method = GET, value = "ra")
    @ResponseBody
    void requestAccess(@RequestParam("name") String name,
                       @RequestParam("email") String email,
                       @RequestParam("description") String description,
                       @RequestParam("username") String username) {
        log.info("entered gscontroller>>>>>>>>>>>>>>>>");
            gsService.requestAccess(name, email, description, username);

    }
}
