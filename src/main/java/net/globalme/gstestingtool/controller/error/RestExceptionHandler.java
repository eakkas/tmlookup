package net.globalme.gstestingtool.controller.error;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@Component
public class RestExceptionHandler extends AbstractHandlerExceptionResolver {
    private final static Logger log = Logger.getLogger(RestExceptionHandler.class);

    final RestErrorResolver resolver = new RestErrorResolver();

    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
                                              Exception ex) {
        ServletWebRequest webRequest = new ServletWebRequest(request, response);
        log.info("handling exception: " + ex.getClass() + ":" + ex.getMessage());
        final RestError restError = resolver.resolveError(webRequest, handler, ex);

        if (!WebUtils.isIncludeRequest(webRequest.getRequest())) {
            webRequest.getResponse().setStatus(restError.getStatus().value());
        }

        return restError.asModelAndView();
    }
}
