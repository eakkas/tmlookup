package net.globalme.gstestingtool.controller.error.exception;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */

public class ForbiddenRestException extends RuntimeException {
    public ForbiddenRestException(final String message) {
        super(message);
    }
}
