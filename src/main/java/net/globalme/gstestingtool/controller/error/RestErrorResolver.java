package net.globalme.gstestingtool.controller.error;

import net.globalme.gstestingtool.controller.error.exception.*;
import org.apache.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import java.io.EOFException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class RestErrorResolver {
    private final static Logger log = Logger.getLogger(RestErrorResolver.class);

    private static Map<String, HttpStatus> exceptionMappings;


    public RestError resolveError(final ServletWebRequest request, final Object handler, final Exception exception) {
        final HttpStatus httpStatus = exceptionMappings.get(exception.getClass().toString());
        if (httpStatus != null) {
            return new RestError(httpStatus, exception.getMessage());
        } else {
            log.error("no mapping for exception: " + exception.getClass(), exception);
            return new RestError(HttpStatus.INTERNAL_SERVER_ERROR, "Internal error");
        }
    }

    private static Map<String, HttpStatus> createExceptionMappingDefinitions() {
        Map<String, HttpStatus> definitions = new LinkedHashMap<String, HttpStatus>();
        //TODO add default message to exceptions with not user-friendly messages  (e.g. Could not read JSON ... - > wrong json format)
        //multipartException -> 400
        //200
        definitions.put(OkRestException.class.toString(), HttpStatus.OK);

        // 400
        definitions.put(HttpMessageNotReadableException.class.toString(), HttpStatus.BAD_REQUEST);
        definitions.put(MissingServletRequestParameterException.class.toString(), HttpStatus.BAD_REQUEST);
        definitions.put(TypeMismatchException.class.toString(), HttpStatus.BAD_REQUEST);
        definitions.put(BadRequestRestException.class.toString(), HttpStatus.BAD_REQUEST);
        definitions.put("javax.validation.ValidationException", HttpStatus.BAD_REQUEST);
        definitions.put(EOFException.class.toString(), HttpStatus.BAD_REQUEST);

        //403
        definitions.put(ForbiddenRestException.class.toString(), HttpStatus.FORBIDDEN);

        // 404
        definitions.put(NoSuchRequestHandlingMethodException.class.toString(), HttpStatus.NOT_FOUND);
        definitions.put(ResourceNotFoundRestException.class.toString(), HttpStatus.NOT_FOUND);
        definitions.put("org.hibernate.ObjectNotFoundException", HttpStatus.NOT_FOUND);

        // 405
        definitions.put(HttpRequestMethodNotSupportedException.class.toString(), HttpStatus.METHOD_NOT_ALLOWED);

        // 406
        definitions.put(HttpMediaTypeNotAcceptableException.class.toString(), HttpStatus.NOT_ACCEPTABLE);

        // 409
        definitions.put("org.springframework.dao.DataIntegrityViolationException", HttpStatus.CONFLICT);

        // 415
        definitions.put(HttpMediaTypeNotSupportedException.class.toString(), HttpStatus.UNSUPPORTED_MEDIA_TYPE);

        //500
        definitions.put(InternalErrorRestException.class.toString(), HttpStatus.INTERNAL_SERVER_ERROR);

        //503
        definitions.put(ServiceUnavailableRestException.class.toString(), HttpStatus.SERVICE_UNAVAILABLE);

        return definitions;
    }

    static {
        exceptionMappings = createExceptionMappingDefinitions();
    }
}
