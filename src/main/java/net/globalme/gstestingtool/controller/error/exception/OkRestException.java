package net.globalme.gstestingtool.controller.error.exception;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */

public class OkRestException extends RuntimeException {
    public OkRestException(final String message) {
        super(message);
    }
}
