package net.globalme.gstestingtool.controller.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class RestError {
    private final HttpStatus status;
    private final String message;

    public RestError(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ModelAndView asModelAndView() {
        MappingJacksonJsonView jsonView = new MappingJacksonJsonView();
        final Map<String, Object> fields = new LinkedHashMap<String, Object>();
        fields.put("status", status.value());
        fields.put("message", message);
        return new ModelAndView(jsonView, fields);
    }

    public String toJsonString() {
        return "{\"status\":" + status.value() + ",\"message\":\"" + message + "\"}";
    }

    @Override
    public String toString() {
        return "RestError{" +
                "status=" + status.value() +
                ", message='" + message + '\'' +
                '}';
    }
}
