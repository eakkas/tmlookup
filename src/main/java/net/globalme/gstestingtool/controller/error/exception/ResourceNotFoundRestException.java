package net.globalme.gstestingtool.controller.error.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class ResourceNotFoundRestException extends RuntimeException {
    public ResourceNotFoundRestException(final String msg) {
        super(msg);
    }
}