package net.globalme.gstestingtool.controller.error.exception;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */

public class BadRequestRestException extends RuntimeException {
    public BadRequestRestException(final String message) {
        super(message);
    }
}
