package net.globalme.gstestingtool.controller.error.exception;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class InternalErrorRestException extends RuntimeException {

    public InternalErrorRestException() {
        super("Internal error.");
    }

    public InternalErrorRestException(final String message) {
        super(message);
    }
}
