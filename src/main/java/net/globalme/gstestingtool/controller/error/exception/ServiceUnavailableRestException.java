package net.globalme.gstestingtool.controller.error.exception;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class ServiceUnavailableRestException extends RuntimeException {
    public ServiceUnavailableRestException(final String message) {
        super(message);
    }
}
