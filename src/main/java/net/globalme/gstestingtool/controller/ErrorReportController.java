package net.globalme.gstestingtool.controller;

import net.globalme.gstestingtool.controller.error.exception.BadRequestRestException;
import net.globalme.gstestingtool.controller.error.exception.OkRestException;
import net.globalme.gstestingtool.entity.ErrorReport;
import net.globalme.gstestingtool.service.ErrorReportService;
import net.globalme.gstestingtool.service.exception.BadArgumentException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@Controller
@RequestMapping("api/report")
public class ErrorReportController {

    private final static Logger log = Logger.getLogger(ErrorReportController.class);

    @Inject
    private ErrorReportService errorReportService;

    @RequestMapping(method = POST, value = "")
    public void report(@RequestBody ErrorReport errorReport) {
        log.info("report method invoked: report: " + errorReport);
        try {
            errorReportService.reportError(errorReport);
        } catch (BadArgumentException e) {
            throw new BadRequestRestException(e.getMessage());
        }
        throw new OkRestException("Thanks for your feedback");
    }
}
