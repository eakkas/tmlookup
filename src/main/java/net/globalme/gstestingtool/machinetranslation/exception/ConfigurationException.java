package net.globalme.gstestingtool.machinetranslation.exception;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class ConfigurationException extends Exception {

    public ConfigurationException(String message) {
        super(message);
    }
}
