package net.globalme.gstestingtool.machinetranslation;

import net.globalme.gstestingtool.entity.GSConfiguration;
import net.globalme.gstestingtool.entity.Term;
import net.globalme.gstestingtool.entity.TranslationMemory;
import net.globalme.gstestingtool.machinetranslation.entity.LocalePair;
import net.globalme.gstestingtool.machinetranslation.exception.ConfigurationException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public interface GlobalSightClient {

    public void configure(final GSConfiguration gsConfiguration) throws ConfigurationException;

    public boolean isConfigured();

    public List<Term> getTermTranslations(final String sourceString, final String sourceLanguage) throws
            IOException;

    public ArrayList<TranslationMemory> getTranslationMemoryMatches(final String sourceString,
                                                                    final String sourceLanguage) throws IOException;

    public ArrayList<LocalePair> getLocalePairs() throws IOException;

    void editTmEntry(final String source, final String sourceLocale, final String target,
                     final String targetLocale) throws IOException;

    void editTbEntry(final String source, final String sourceLocale, final String target,
                     final String targetLocale) throws IOException;

    void deleteTmEntry(final String source, final String sourceLocale, final String targetLocale) throws IOException;

    void deleteTbEntry(final String source, final String sourceLocale, final String targetLocale) throws IOException;

    void requestAccess(final String name, final String email, final String description, final String username);
}
