package net.globalme.gstestingtool.machinetranslation;

import net.globalme.gstestingtool.entity.GSConfiguration;
import net.globalme.gstestingtool.machinetranslation.exception.ConfigurationException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import static java.lang.Boolean.parseBoolean;
import static java.lang.Integer.parseInt;
import static net.globalme.gstestingtool.Config.APP_NAME;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@Component
public class FileConfigurationProvider implements ConfigurationProvider {
    private final static Logger log = Logger.getLogger(FileConfigurationProvider.class);
    private final static String PATH_CONFIGURATION_FILE = "/home/ubuntu/" + APP_NAME + "/config/configuration" +
            ".properties";
//	 private final static String PATH_CONFIGURATION_FILE = "/configuration.properties";
    private static final String PROPERTY_NAME_HOST = "host";
    private static final String PROPERTY_NAME_PORT = "port";
//    private static final String PROPERTY_NAME_USERNAME = "username";
//    private static final String PROPERTY_NAME_PASSWORD = "password";
    private static final String PROPERTY_NAME_TERMBASE_NAME = "termbaseName";
    private static final String PROPERTY_NAME_TM_PROFILE_NAME = "tmProfileName";
    private static final String PROPERTY_NAME_ALLOW_ENTRIES_DELETION = "allowDeletion";
    private static final String PROPERTY_NAME_ONLY_ENGLISH_IN_SOURCE = "onlyEnglishInSource";
    private static final String PROPERTY_NAME_ALLOWED_GROUP = "allowedPermissionGroup";

    @Override
    public GSConfiguration getConfiguration() throws ConfigurationException {
        final Properties configuration = new Properties();
        try {
            configuration.load(new FileInputStream(PATH_CONFIGURATION_FILE));
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException while reading configuration file", e);
            throw new ConfigurationException("Configuration file not found. Please make sure that it is in the " +
                    "right place and restart the app");
        } catch (IOException e) {
            log.error("IOException while loading configuration file", e);
            throw new ConfigurationException("Error while loading configuration file. Please check logs for details.");
        }
        checkConfigurationProperty(configuration, PROPERTY_NAME_HOST);
        checkIntegerConfigurationProperty(configuration, PROPERTY_NAME_PORT);
//        checkConfigurationProperty(configuration, PROPERTY_NAME_USERNAME);
//        checkConfigurationProperty(configuration, PROPERTY_NAME_PASSWORD);
        checkConfigurationProperty(configuration, PROPERTY_NAME_TERMBASE_NAME);
        checkConfigurationProperty(configuration, PROPERTY_NAME_TM_PROFILE_NAME);
        checkBooleanConfigurationProperty(configuration, PROPERTY_NAME_ALLOW_ENTRIES_DELETION);
        checkBooleanConfigurationProperty(configuration, PROPERTY_NAME_ONLY_ENGLISH_IN_SOURCE);

        return new GSConfiguration(configuration.getProperty(PROPERTY_NAME_HOST),
                parseInt(configuration.getProperty(PROPERTY_NAME_PORT)),
//                configuration.getProperty(PROPERTY_NAME_USERNAME), configuration.getProperty(PROPERTY_NAME_PASSWORD),
                configuration.getProperty(PROPERTY_NAME_TERMBASE_NAME),
                configuration.getProperty(PROPERTY_NAME_TM_PROFILE_NAME),
                parseBoolean(configuration.getProperty(PROPERTY_NAME_ALLOW_ENTRIES_DELETION)),
                parseBoolean(configuration.getProperty(PROPERTY_NAME_ONLY_ENGLISH_IN_SOURCE)),
                configuration.getProperty(PROPERTY_NAME_ALLOWED_GROUP));
    }

    private void checkBooleanConfigurationProperty(final Properties configuration,
                                                   final String propertyName) throws ConfigurationException {
        checkConfigurationProperty(configuration, propertyName);
        final String propertyValue = configuration.getProperty(propertyName);
        if (!"true".equalsIgnoreCase(propertyValue) && !"false".equalsIgnoreCase(propertyValue)) {
            throw new ConfigurationException(String.format("Error while loading configuration file. The \"%s\" " +
                    "property is not a boolean", propertyName));
        }
    }

    private void checkIntegerConfigurationProperty(final Properties configuration,
                                                   final String propertyName) throws ConfigurationException {
        checkConfigurationProperty(configuration, propertyName);
        try {
            parseInt(configuration.getProperty(PROPERTY_NAME_PORT));
        } catch (NumberFormatException e) {
            throw new ConfigurationException(String.format("Error while loading configuration file. The \"%s\" " +
                    "property is not an integer", propertyName));
        }
    }

    private void checkConfigurationProperty(final Properties configuration, final String propertyName) throws ConfigurationException {
        if (!configuration.containsKey(propertyName)) {
            throw new ConfigurationException(String.format("\"%s\" property is not present in the configuration " +
                    "file", propertyName));
        }
    }
}
