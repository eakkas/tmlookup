package net.globalme.gstestingtool.machinetranslation.mappers;

import net.globalme.gstestingtool.entity.Term;
import net.globalme.gstestingtool.machinetranslation.entity.TbEntry;
import org.springframework.jdbc.core.RowMapper;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by nerseszakoyan on 10.12.15.
 */
public class TermMapper implements RowMapper<Term> {

    private final XPathExpression partOfSpeechExpr;
    private final XPathExpression domainExpr;
    private final XPathExpression contextExpr;
    private final XPathExpression definitionExpr;
    private DocumentBuilder builder;

    public TermMapper(DocumentBuilder builder) throws XPathExpressionException {
        this.builder = builder;
        XPath xpath = XPathFactory.newInstance().newXPath();
        partOfSpeechExpr = xpath.compile("//descrip[@type='text-part_of speech']/text()");
        domainExpr = xpath.compile("//descrip[@type='text-domain']/text()");
        contextExpr = xpath.compile("//descrip[@type='text-context']/text()");
        definitionExpr  = xpath.compile("//descrip[@type='text-definition']/text()");
    }

    public Term mapRow(ResultSet rs, int rowNum) throws SQLException{
        Term term = new Term();
        term.setLocale(rs.getString("lang_name"));
        term.setSource(rs.getString("source_term"));
        term.setTranslation(rs.getString("term"));

        String xml = "<root>" + rs.getString("xml") + "</root>";

        Document doc = null;
        try {
            doc = builder.parse(new InputSource(new StringReader(xml)));
            term.setPartOfSpeech(partOfSpeechExpr.evaluate(doc));
            term.setDomain(domainExpr.evaluate(doc));
            term.setContext(contextExpr.evaluate(doc));
            term.setDefinition(definitionExpr.evaluate(doc));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        System.out.println(">>>>>>"+term.toString());
        return term;
    }
}