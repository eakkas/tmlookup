package net.globalme.gstestingtool.machinetranslation;

import net.globalme.gstestingtool.entity.GSConfiguration;
import net.globalme.gstestingtool.entity.Term;
import net.globalme.gstestingtool.entity.TranslationMemory;
import net.globalme.gstestingtool.machinetranslation.dao.TermTemplate;
import net.globalme.gstestingtool.machinetranslation.entity.*;
import net.globalme.gstestingtool.machinetranslation.entity.Locale;
import net.globalme.gstestingtool.machinetranslation.exception.ConfigurationException;
import net.globalme.gstestingtool.service.ApplicationMailer;
import net.globalme.gstestingtool.service.GSService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.*;
import javax.xml.xpath.XPathExpressionException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.globalme.gstestingtool.util.LocaleUtil.renameLanguageIfNeeded;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@Component
public class GlobalSightClientImpl implements GlobalSightClient, AuthenticationProvider {

    @Autowired
    private ApplicationContext appContext;

    @Inject
    private GSService gsService;

    private final static String AMBASSADOR_SERVICE_PATH = "/globalsight/services/AmbassadorWebService";

    private final static Logger log = Logger.getLogger(GlobalSightClientImpl.class);

    private final static String PARAMETER_NAME_USERNAME = "username";
    private final static String PARAMETER_NAME_PASSWORD = "password";
    private final static String PARAMETER_NAME_ACCESS_TOKEN = "accessToken";
    private final static String PARAMETER_NAME_TM_PROFILE_NAME = "tmProfileName";
    private final static String PARAMETER_NAME_TM_SEARCH_STRING = "string";
    private final static String PARAMETER_NAME_TERMBASE_NAME = "termbaseName";
    private final static String PARAMETER_NAME_TB_SEARCH_STRING = "searchString";
    private final static String PARAMETER_NAME_SOURCE_LOCALE = "sourceLocale";
    private final static String PARAMETER_NAME_TARGET_LOCALE = "targetLocale";
    private final static String PARAMETER_NAME_TB_MATCH_TYPE = "matchType";
    private final static String PARAMETER_NAME_PERMISSION_NAME = "permissionName";

    private final static String METHOD_NAME_LOGIN = "login";
    private final static String METHOD_NAME_SEARCH_TB = "searchTBEntries";
    private final static String METHOD_NAME_SEARCH_TM = "searchEntries";
    private final static String METHOD_NAME_GET_ALL_LOCALE_PAIRS = "getAllLocalePairs";
    private final static String METHOD_NAME_IS_EXISTED_PERMISSION = "isExistedPermission";

    private final static String ENCODING_UTF_8 = "UTF-8";

    private String accessToken = null;

    private GSConfiguration gsConfiguration;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        gsConfiguration.setUsername(name);
        gsConfiguration.setPassword(password);
        try {
        accessToken = getNewAccessToken(gsConfiguration);
        if (accessToken == null) {
            return null;
        } else {
            Boolean hasPermission = isExistedPermission(gsConfiguration, accessToken);
            if (hasPermission == null) {
                return null;
            } else if (!hasPermission) {
                return new UsernamePasswordAuthenticationToken(name, password, null);
            } else {
                try {
                    log.info("getting available locales");
                    gsService.setAvailableLocales(mergeLocales(getLocalePairs(), gsConfiguration.isOnlyEnglishInSource()));
                    sortTargetLocales(gsService.getAvailableLocales());
                    log.info("getting available locales: done: available locales: " + gsService.getAvailableLocales());
                } catch (IOException e) {
                    log.error("error while getting available locales", e);
                    gsService.setInitializingError("Error occurred while initializing app. Please check logs for more details.");
                    return null;
                }
                List<GrantedAuthority> grantedAuths = new ArrayList<>();
                grantedAuths.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                Authentication auth = new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
                return auth;
            }
        }
        } catch (IOException e) {
            log.error("Error while validating configuration. Please check logs for details");
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    public void configure(final GSConfiguration gsConfiguration)  {
        checkNotNull(gsConfiguration);

        this.gsConfiguration = gsConfiguration;
    }

    private static String getNewAccessToken(final GSConfiguration gsConfiguration) throws IOException {
        log.info("getting new access token with configuration: " + gsConfiguration);
        final Map<String, String> loginParams = new LinkedHashMap<String, String>();
        loginParams.put(PARAMETER_NAME_USERNAME, gsConfiguration.getUsername());
        loginParams.put(PARAMETER_NAME_PASSWORD, gsConfiguration.getPassword());
        try {
            final String newAccessToken = makeCall(METHOD_NAME_LOGIN, loginParams, getAmbassadorServiceUrl(gsConfiguration));
            if (newAccessToken.contains("errorXml")) {
                log.error("error while getting access token: " + newAccessToken);
                return null;
            }
            return newAccessToken;
        } catch (SOAPException e) {
            log.error("SOAPException while getting access token: " + e.getMessage());
            throw new IOException(e);
        } catch (IOException e) {
            log.error("IOException while getting access token: " + e.getMessage());
            throw new IOException(e);
        } catch (Exception e) {
            log.error("Exception while getting access token: " + e.getMessage());
            throw new IOException(e);
        }
    }

    private static Boolean isExistedPermission(final GSConfiguration gsConfiguration, String accessToken) throws IOException {
        log.info("checking permission with configuration: " + gsConfiguration);
        final Map<String, String> checkParams = new LinkedHashMap<String, String>();
        checkParams.put(PARAMETER_NAME_ACCESS_TOKEN, accessToken);
        checkParams.put(PARAMETER_NAME_PERMISSION_NAME, gsConfiguration.getAllowedPermissionGroup());
        try {
            final String hasPermission = makeCall(METHOD_NAME_IS_EXISTED_PERMISSION, checkParams, getAmbassadorServiceUrl(gsConfiguration));
            if (hasPermission.contains("errorXml")) {
                log.error("error while checking permission: " + hasPermission);
                return null;
            }
            return Boolean.parseBoolean(hasPermission);
        } catch (SOAPException e) {
            log.error("SOAPException while checking permission: " + e.getMessage());
            throw new IOException(e);
        } catch (IOException e) {
            log.error("IOException while checking permission: " + e.getMessage());
            throw new IOException(e);
        } catch (Exception e) {
            log.error("Exception while checking permission: " + e.getMessage());
            throw new IOException(e);
        }
    }

    private static Map<Locale, ArrayList<Locale>> mergeLocales(final ArrayList<LocalePair> localePairs,
                                                               final boolean onlyEnglishInSource) {
        final Map<Locale, ArrayList<Locale>> result = new HashMap<Locale, ArrayList<Locale>>();
        for (final LocalePair localePair : localePairs) {

            localePair.setSourceLocale(renameLanguageIfNeeded(localePair.getSourceLocale()));
            localePair.setTargetLocale(renameLanguageIfNeeded(localePair.getTargetLocale()));

            if (onlyEnglishInSource && !localePair.getSourceLocale().getCode().equals("en_US")) {
                continue;
            }

            if (!result.containsKey(localePair.getSourceLocale())) {
                result.put(localePair.getSourceLocale(), new ArrayList<Locale>());
            }
            result.get(localePair.getSourceLocale()).add(localePair.getTargetLocale());
        }
        return result;
    }

    private void checkAccessToken() throws IOException {
        if (accessToken == null) {
            accessToken = getNewAccessToken(gsConfiguration);
        }
    }

    public boolean isConfigured() {
        return gsConfiguration != null;
    }

    public List<Term> getTermTranslations(final String sourceString, final String sourceLanguage) throws IOException {

        TermTemplate termTemplate = (TermTemplate)appContext.getBean("termTemplate");
        try {
            return termTemplate.getResults(gsConfiguration.getTermbaseName(), sourceString, sourceLanguage);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        return new ArrayList<Term>();
//        checkAccessToken();
//        try {
//            final ArrayList<TbEntry> tbEntries = getTbEntries(sourceString, sourceLanguage);
//            return convertTbEntriesToTerms(tbEntries);
//        } catch (IOException e) {
//            if (accessToken == null) {
//                throw e;
//            }
//            accessToken = null;
//            return getTermTranslations(sourceString, sourceLanguage);
//        }
    }

    public ArrayList<TranslationMemory> getTranslationMemoryMatches(final String sourceString,
                                                                    final String sourceLanguage) throws IOException {
        log.info("getTranslationMemoryMatches: checking access token");
        checkAccessToken();
        try {
            log.info("getTranslationMemoryMatches: getting tm entries for sourceString: " + sourceString + ", " +
                    "sourceLanguage: " + sourceLanguage);
            final ArrayList<TmEntry> tmEntries = getTmEntries(sourceString, sourceLanguage);
            log.info("getTranslationMemoryMatches: removing unrelated matches");
            removeUnrelatedTmMatches(tmEntries);
            log.info("returning tm matches converted from tm entries");
            return convertTmEntriesToTranslationMemoryMatches(tmEntries);
        } catch (IOException e) {
            if (accessToken == null) {
                throw e;
            }
            accessToken = null;
            return getTranslationMemoryMatches(sourceString, sourceLanguage);
        }

    }

    private void removeUnrelatedTmMatches(final ArrayList<TmEntry> tmEntries) {
        if (tmEntries.isEmpty()) {
            return;
        }
        for (Iterator<TmEntry> iterator = tmEntries.iterator(); iterator.hasNext(); ) {
            TmEntry tmEntry = iterator.next();
            if (tmEntry.getTarget() == null
                    || tmEntry.getSource() == null
                    || "0%".equals(tmEntry.getPercentage())
                    || "".equals(tmEntry.getTarget().getSegment())) {
                log.info("removing unrelated match: " + tmEntry);
                iterator.remove();
            }
        }
    }


    public ArrayList<LocalePair> getLocalePairs() throws IOException {
        final Map<String, String> params = new LinkedHashMap<String, String>();
        params.put(PARAMETER_NAME_ACCESS_TOKEN, accessToken);
        try {
            final String response = makeCall(METHOD_NAME_GET_ALL_LOCALE_PAIRS, params, getAmbassadorServiceUrl(gsConfiguration));
            final Unmarshaller unmarshaller = JAXBContext.newInstance(
                    LocalePairs.class).createUnmarshaller();
            final LocalePairs localePairs = (LocalePairs) unmarshaller.unmarshal(new ByteArrayInputStream(response
                    .getBytes("UTF-8")));
            if (localePairs.getLocalePairs() == null) {
                return new ArrayList<LocalePair>();
            } else {
                return new ArrayList<LocalePair>(localePairs.getLocalePairs());
            }
        } catch (SOAPException e) {
            throw new IOException(e);
        } catch (JAXBException e) {
            throw new IOException(e);
        }
    }


    public void editTmEntry(final String source, final String sourceLocale, final String target,
                            final String targetLocale) throws IOException {
        //todo refactoring: use this code only in one method
        checkAccessToken();
        try {
            makeEditTmEntryCall(source, sourceLocale, target, targetLocale);
        } catch (IOException e) {
            if (accessToken == null) {
                throw e;
            }
            accessToken = null;
            makeEditTmEntryCall(source, sourceLocale, target, targetLocale);
        }
    }

    public void editTbEntry(final String source, final String sourceLocale, final String target,
                            final String targetLocale) throws IOException {
        checkAccessToken();
        try {
            makeEditTmEntryCall(source, sourceLocale, target, targetLocale);
        } catch (IOException e) {
            if (accessToken == null) {
                throw e;
            }
            accessToken = null;
            makeEditTmEntryCall(source, sourceLocale, target, targetLocale);
        }
    }

    public void deleteTmEntry(String source, String sourceLocale, String targetLocale) throws IOException {
        checkAccessToken();
        try {
            makeDeleteTmEntryCall(source, sourceLocale, targetLocale);
        } catch (IOException e) {
            if (accessToken == null) {
                throw e;
            }
            accessToken = null;
            makeDeleteTmEntryCall(source, sourceLocale, targetLocale);
        }
    }

    public void deleteTbEntry(String source, String sourceLocale, String targetLocale) throws IOException {
        checkAccessToken();
        try {
            makeDeleteTbEntryCall(source, sourceLocale, targetLocale);
        } catch (IOException e) {
            if (accessToken == null) {
                throw e;
            }
            accessToken = null;
            makeDeleteTbEntryCall(source, sourceLocale, targetLocale);
        }
    }

    @Override
    public void requestAccess(String name, String email, String description, String username) {
        ApplicationMailer mailer = (ApplicationMailer) appContext.getBean("mailMail");

        //Send a pre-configured mail

        mailer.sendPreConfiguredMail(name, email, description, username);
    }

    private void makeDeleteTmEntryCall(final String source, final String sourceLocale,
                                       final String targetLocale) throws IOException {
        //todo use string constants
        //todo test
        //todo throw exception in case of request fail
        final Map<String, String> params = new LinkedHashMap<String, String>();
        params.put(PARAMETER_NAME_ACCESS_TOKEN, accessToken);
        params.put("tmProfileName", gsConfiguration.getTmProfileName());
        params.put("string", source);
        params.put("sourceLocale", sourceLocale);
        params.put("localeToDelete", targetLocale);

        try {
            final String response = makeCall("deleteSegment", params, getAmbassadorServiceUrl(gsConfiguration));
            log.info("makeDeleteTmEntryCall: " + response);
        } catch (SOAPException e) {
            throw new IOException(e);
        }
    }

    private void makeDeleteTbEntryCall(final String source, final String sourceLocale,
                                       final String targetLocale) throws IOException {
        //todo use string constants
        //todo test
        //todo throw exception in case of request fail
        final Map<String, String> params = new LinkedHashMap<String, String>();
        params.put(PARAMETER_NAME_ACCESS_TOKEN, accessToken);
        params.put("termbaseName", gsConfiguration.getTermbaseName());
        params.put("searchString", source);
        params.put("sourceLocale", sourceLocale);
        params.put("targetLocale", targetLocale);

        try {
            final String response = makeCall("deleteTBEntry", params, getAmbassadorServiceUrl(gsConfiguration));
            log.info("makeDeleteTbEntryCall: " + response);
        } catch (SOAPException e) {
            throw new IOException(e);
        }
    }

    private void makeEditTmEntryCall(final String source, final String sourceLocale, final String target,
                                     final String targetLocale) throws IOException {
        //todo use string constants
        //todo test
        //todo throw exception in case of request fail
        final Map<String, String> params = new LinkedHashMap<String, String>();
        params.put(PARAMETER_NAME_ACCESS_TOKEN, accessToken);
        params.put("tmProfileName", gsConfiguration.getTmProfileName());
        params.put("sourceLocale", sourceLocale);
        params.put("sourceSegment", source);
        params.put("targetLocale", targetLocale);
        params.put("targetSegment", target);

        try {
            final String response = makeCall("editEntry", params, getAmbassadorServiceUrl(gsConfiguration));
            log.info("makeEditTmEntryCall: " + response);
        } catch (SOAPException e) {
            throw new IOException(e);
        }

    }

    private void makeEditTbEntryCall(final String source, final String sourceLocale, final String target,
                                     final String targetLocale) throws IOException {
        //todo use string constants
        //todo test
        //todo throw exception in case of request fail
        final Map<String, String> params = new LinkedHashMap<String, String>();
        params.put(PARAMETER_NAME_ACCESS_TOKEN, accessToken);
        params.put("termbaseName", gsConfiguration.getTermbaseName());
        params.put("sourceLocale", sourceLocale);
        params.put("sourceTerm", source);
        params.put("targetLocale", targetLocale);
        params.put("targetTerm", target);

        try {
            final String response = makeCall("editTBEntry", params, getAmbassadorServiceUrl(gsConfiguration));
            log.info("makeEditTbEntryCall: " + response);
        } catch (SOAPException e) {
            throw new IOException(e);
        }

    }

    private static String getAmbassadorServiceUrl(final GSConfiguration gsConfiguration) {
        checkNotNull(gsConfiguration.getHost());
        checkNotNull(gsConfiguration.getPort());
        return gsConfiguration.getHost() + ":" + gsConfiguration.getPort() + AMBASSADOR_SERVICE_PATH;
    }


    private static ArrayList<Term> convertTbEntriesToTerms(final List<TbEntry> tbEntries) {
        final ArrayList<Term> terms = new ArrayList<Term>();
//        for (final TbEntry tbEntry : tbEntries) {
//            for (final TbEntry.Term term : tbEntry.getTerms()) {
//                if (!term.isSrc()) {
//                    terms.add(new Term(tbEntry.getSourceTermContent(), term.getTermContent(), term.getLangName()));
//                }
//            }
//        }
        return terms;
    }

    private static ArrayList<TranslationMemory> convertTmEntriesToTranslationMemoryMatches(List<TmEntry> tmEntries) {
        final ArrayList<TranslationMemory> translationMemoryObjects = new ArrayList<TranslationMemory>();
        for (final TmEntry tmEntry : tmEntries) {
            translationMemoryObjects.add(new TranslationMemory(tmEntry.getSourceContent(), tmEntry.getTargetContent(),
                    tmEntry.getTarget().getLocale(), tmEntry.getPercentageValue(), tmEntry.getSource().getLocale()));
        }
        return translationMemoryObjects;
    }

    private ArrayList<TmEntry> getTmEntries(final String sourceString, final String sourceLanguage) throws IOException {
        try {
            final Map<String, String> tmParams = new LinkedHashMap<String, String>();
            tmParams.put(PARAMETER_NAME_ACCESS_TOKEN, accessToken);
            tmParams.put(PARAMETER_NAME_TM_PROFILE_NAME, gsConfiguration.getTmProfileName());
            tmParams.put(PARAMETER_NAME_TM_SEARCH_STRING, sourceString);
            tmParams.put(PARAMETER_NAME_SOURCE_LOCALE, sourceLanguage);
            final String response = makeCall(METHOD_NAME_SEARCH_TM, tmParams,
                    getAmbassadorServiceUrl(gsConfiguration));
            final Unmarshaller unmarshaller = JAXBContext.newInstance(
                    TmEntries.class).createUnmarshaller();
            ByteArrayInputStream input = new ByteArrayInputStream(response.getBytes(ENCODING_UTF_8));
            final TmEntries tmEntries = (TmEntries) unmarshaller.unmarshal(input);
            if (tmEntries.getTmEntries() == null) {
                return new ArrayList<TmEntry>();
            } else {
                return new ArrayList<TmEntry>(tmEntries.getTmEntries());
            }
        } catch (JAXBException e) {
            throw new IOException(e);
        } catch (SOAPException e) {
            throw new IOException(e);
        }
    }

    //TODO rename languages to locale codes
    private ArrayList<TbEntry> getTbEntries(final String sourceString, final String sourceLanguage) throws IOException {
        String response = null;
        try {

//            TermTemplate tbEntryTemplate =
//                    (TermTemplate)appContext.getBean("tbEntryTemplate");
//
//            SqlRowSet sqlRowSet = tbEntryTemplate.getJdbcTemplateObject().queryForRowSet("select * from tb_term where term='tipo de cuenta' and lang_name='Spanish - AR'");
//            while (sqlRowSet.next()) {
//                System.out.println(">>>>>>>>>>>>>>>>>"+sqlRowSet.getInt("tbid"));
//            }
            System.out.println("boooooooo"+sourceLanguage);
            final Map<String, String> tbParams = new LinkedHashMap<String, String>();
            tbParams.put(PARAMETER_NAME_ACCESS_TOKEN, accessToken);
            tbParams.put(PARAMETER_NAME_TERMBASE_NAME, gsConfiguration.getTermbaseName());
            tbParams.put(PARAMETER_NAME_TB_SEARCH_STRING, sourceString);
            tbParams.put(PARAMETER_NAME_SOURCE_LOCALE, sourceLanguage);
            tbParams.put(PARAMETER_NAME_TARGET_LOCALE, "");
            tbParams.put(PARAMETER_NAME_TB_MATCH_TYPE, "2");
            response = makeCall(METHOD_NAME_SEARCH_TB, tbParams, getAmbassadorServiceUrl(gsConfiguration));

            final Unmarshaller unmarshaller = JAXBContext.newInstance(
                    TbEntries.class).createUnmarshaller();
            ByteArrayInputStream input = new ByteArrayInputStream(response.getBytes(ENCODING_UTF_8));
            final TbEntries tbEntries = (TbEntries) unmarshaller.unmarshal(input);
            if (tbEntries.getTbEntries() == null) {
                return new ArrayList<TbEntry>();
            } else {
                return new ArrayList<TbEntry>(tbEntries.getTbEntries());
            }
        } catch (JAXBException e) {
            throw new IOException("JAXBException: response: " + response + ", message: " + e.getMessage());
        } catch (SOAPException e) {
            throw new IOException("SOAPException: " + e.getMessage());
        }
    }

    //todo use custom error object
    private static String makeCall(final String action, final Map<String, String> params, final String endpoint) throws SOAPException, IOException {
        log.info("making call: action: " + action + ", params: " + params + ", endpoint: " + endpoint);
        SOAPMessage message = MessageFactory.newInstance().createMessage();
        SOAPHeader header = message.getSOAPHeader();
        header.detachNode();

        SOAPBody body = message.getSOAPBody();
        QName bodyName = new QName(action);
        SOAPBodyElement bodyElement = body.addBodyElement(bodyName);
        for (final String key : params.keySet()) {
            bodyElement.addChildElement(key).addTextNode(params.get(key));
        }
        getXmlFromSOAPMessage(message);

        SOAPConnection connection = SOAPConnectionFactory.newInstance().createConnection();
        SOAPMessage response = connection.call(message, endpoint);
        connection.close();

        SOAPBody responseBody = response.getSOAPBody();
        log.info("response body: " + responseBody.getValue());
        SOAPBodyElement responseElement = (SOAPBodyElement) responseBody.getChildElements().next();

        SOAPElement returnElement = (SOAPElement) responseElement.getChildElements().next();
        if (responseBody.getFault() != null) {
            log.warn("error response: fault value: " + responseBody.getFault().getValue() + ", " +
                    "faultString: " + responseBody.getFault().getFaultString());
            return returnElement.getValue() + " " + responseBody.getFault().getFaultString();
        }
        log.info("response string: " + returnElement.getValue());

        return returnElement.getValue();
    }

    private static String getXmlFromSOAPMessage(SOAPMessage msg) throws SOAPException, IOException {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        msg.writeTo(byteArrayOS);
        return new String(byteArrayOS.toByteArray());
    }

    private void sortTargetLocales(final Map<Locale, ArrayList<Locale>> localesMap) {
        for (final ArrayList<Locale> targetLocales : localesMap.values()) {
            Collections.sort(targetLocales);
        }
    }


}
