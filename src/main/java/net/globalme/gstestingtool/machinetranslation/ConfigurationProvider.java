package net.globalme.gstestingtool.machinetranslation;

import net.globalme.gstestingtool.entity.GSConfiguration;
import net.globalme.gstestingtool.machinetranslation.exception.ConfigurationException;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public interface ConfigurationProvider {
    public GSConfiguration getConfiguration() throws ConfigurationException;
}
