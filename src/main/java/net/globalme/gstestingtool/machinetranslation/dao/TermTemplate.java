package net.globalme.gstestingtool.machinetranslation.dao;

/**
 * Created by nerseszakoyan on 10.12.15.
 */

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import net.globalme.gstestingtool.entity.Term;
import net.globalme.gstestingtool.machinetranslation.mappers.TermMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

public class TermTemplate implements TermDAO {
    private DataSource dataSource;
    private NamedParameterJdbcTemplate jdbcTemplateObject;


    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new NamedParameterJdbcTemplate(dataSource);
    }

    public NamedParameterJdbcTemplate getJdbcTemplateObject() {
        return jdbcTemplateObject;
    }

    public List<Term> getResults(String tbName, String searchText, String lang) throws XPathExpressionException, ParserConfigurationException {

        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true);
        DocumentBuilder builder = domFactory.newDocumentBuilder();
//
        System.out.println(">>>>>>>>>>>>>>>>>"+tbName+"<<<<<<<<<<<<<<");
        System.out.println(">>>>>>>>>>>>>>>>>"+searchText+"<<<<<<<<<<<<<<");
        System.out.println(">>>>>>>>>>>>>>>>>"+lang+"<<<<<<<<<<<<<<");
        //tb_termbase name,   name.tb_name=? and name.tbid=a.tbid and


        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("search", "%"+searchText+"%");
        params.addValue("name", tbName);
        params.addValue("lang", "English - US");
        String SQL = "select a.term as source_term, b.term, b.lang_name, c.xml from tb_termbase name, tb_term a, tb_term b, tb_concept c where name.tb_name=:name and name.tbid=a.tbid and a.term like :search and a.lang_name=:lang and a.cid=b.cid and c.cid=b.cid";
        return jdbcTemplateObject.query(SQL, params, new TermMapper(builder));//, tbName, lang
    }
}