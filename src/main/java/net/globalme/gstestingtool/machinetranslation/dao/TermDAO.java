package net.globalme.gstestingtool.machinetranslation.dao;

/**
 * Created by nerseszakoyan on 10.12.15.
 */
import net.globalme.gstestingtool.machinetranslation.entity.TbEntry;

import java.util.List;
import javax.sql.DataSource;

public interface TermDAO {
    /**
     * This is the method to be used to initialize
     * database resources ie. connection.
     */
    public void setDataSource(DataSource ds);


}