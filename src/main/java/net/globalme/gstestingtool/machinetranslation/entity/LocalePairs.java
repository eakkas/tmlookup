package net.globalme.gstestingtool.machinetranslation.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "LocalePairInformation")
public class LocalePairs {
    @XmlElement(name = "localePair")
    private List<LocalePair> localePairs;

    public LocalePairs() {
    }

    public List<LocalePair> getLocalePairs() {
        return localePairs;
    }

    public void setLocalePairs(List<LocalePair> localePairs) {
        this.localePairs = localePairs;
    }

    @Override
    public String toString() {
        return "LocalePairs{" +
                "localePairs=" + localePairs +
                '}';
    }
}
