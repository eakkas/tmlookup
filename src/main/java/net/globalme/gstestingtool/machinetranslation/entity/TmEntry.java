package net.globalme.gstestingtool.machinetranslation.entity;

import com.google.common.base.Joiner;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@XmlAccessorType(XmlAccessType.NONE)
public class TmEntry {

    @JsonIgnore
    @XmlElement
    private String percentage;

    @JsonIgnore
    @XmlElement(name = "source")
    private Value source;

    @JsonIgnore
    @XmlElement(name = "target")
    private Value target;

    public TmEntry() {
    }

    @JsonProperty(value = "percentage")
    public double getPercentageValue() {
        return Double.parseDouble(percentage.replace("%", ""));
    }

    @JsonProperty(value = "source")
    public String getSourceContent() {
        return Joiner.on("").join(source.getSegment().getText());
    }

    @JsonProperty(value = "target")
    public String getTargetContent() {
        return Joiner.on("").join(target.getSegment().getText());
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public Value getSource() {
        return source;
    }

    public void setSource(Value source) {
        this.source = source;
    }

    public Value getTarget() {
        return target;
    }

    public void setTarget(Value target) {
        this.target = target;
    }

    @Override
    public String toString() {
        return "TmEntry{" +
                "percentage='" + percentage + '\'' +
                ", source=" + source +
                ", target=" + target +
                '}';
    }

    @XmlAccessorType(XmlAccessType.NONE)
    public static class Value {
        @XmlElement
        private String locale;

        @JsonIgnore
        @XmlElement
        private Segment segment;

        Value() {
        }

        public String getLocale() {
            return locale;
        }

        public void setLocale(String locale) {
            this.locale = locale;
        }

        public Segment getSegment() {
            return segment;
        }

        public void setSegment(Segment segment) {
            this.segment = segment;
        }

        @Override
        public String toString() {
            return "Value{" +
                    "locale='" + locale + '\'' +
                    ", segment='" + segment + '\'' +
                    '}';
        }
    }

    @XmlRootElement
    @XmlAccessorType(XmlAccessType.NONE)
    public static class Segment {
        private List<Object> mixedContent = new ArrayList<Object>();
        private List<String> text;

        @XmlElementRefs({
                @XmlElementRef(name = "bpt", type = AnyElement.class),
                @XmlElementRef(name = "ept", type = AnyElement.class)})
        public List<Object> getMixedContent() {
            return mixedContent;
        }

        public void setMixedContent(List<Object> mixedContent) {
            this.mixedContent = mixedContent;
        }

        @XmlMixed
        public List<String> getText() {
            return text;
        }

        public void setText(List<String> text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return "Segment{" +
                    "mixedContent=" + mixedContent +
                    ", text=" + text +
                    '}';
        }
    }
}
