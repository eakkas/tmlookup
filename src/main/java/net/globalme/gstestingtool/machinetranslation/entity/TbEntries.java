package net.globalme.gstestingtool.machinetranslation.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class TbEntries {

    @XmlElement(name = "tbEntry")
    private List<TbEntry> tbEntries;

    public TbEntries() {
    }

    public List<TbEntry> getTbEntries() {
        return tbEntries;
    }

    public void setTbEntries(List<TbEntry> tbEntries) {
        this.tbEntries = tbEntries;
    }

    @Override
    public String toString() {
        return "TbEntries{" +
                "tbEntries=" + tbEntries +
                '}';
    }
}
