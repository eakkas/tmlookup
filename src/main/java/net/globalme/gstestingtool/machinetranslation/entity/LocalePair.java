package net.globalme.gstestingtool.machinetranslation.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@XmlAccessorType(XmlAccessType.NONE)
public class LocalePair {
    @XmlElement
    private Long id;

    @XmlElement
    private Locale sourceLocale;

    @XmlElement
    private Locale targetLocale;

    public LocalePair() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Locale getSourceLocale() {
        return sourceLocale;
    }

    public void setSourceLocale(Locale sourceLocale) {
        this.sourceLocale = sourceLocale;
    }

    public Locale getTargetLocale() {
        return targetLocale;
    }

    public void setTargetLocale(Locale targetLocale) {
        this.targetLocale = targetLocale;
    }

    @Override
    public String toString() {
        return "LocalePair{" +
                "id=" + id +
                ", sourceLocale=" + sourceLocale +
                ", targetLocale=" + targetLocale +
                '}';
    }
}
