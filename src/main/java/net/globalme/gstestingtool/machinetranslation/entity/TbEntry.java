package net.globalme.gstestingtool.machinetranslation.entity;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@XmlAccessorType(XmlAccessType.NONE)
public class TbEntry {

    @JsonIgnore
    @XmlElement(name = "tbName")
    private String termBaseName;

    @JsonIgnore
    @XmlElement(name = "term")
    private List<Term> terms;

    public TbEntry() {
    }

    @JsonProperty(value = "source")
    public String getSourceTermContent() {
        return getSourceTerm().getTermContent();
    }

    @JsonProperty(value = "target")
    public String getTargetTermContent() {
        return getTargetTerm().getTermContent();
    }

    @JsonIgnore
    public Term getSourceTerm() {
        for (final Term term : terms) {
            if (term.isSrc()) {
                return term;
            }
        }
        return null;
    }

    @JsonIgnore
    public Term getTargetTerm() {
        for (final Term term : terms) {
            if (!term.isSrc()) {
                return term;
            }
        }
        return null;
    }

    public String getTermBaseName() {
        return termBaseName;
    }

    public void setTermBaseName(String termBaseName) {
        this.termBaseName = termBaseName;
    }

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }

    @Override
    public String toString() {
        return "TbEntry{" +
                "termBaseName='" + termBaseName + '\'' +
                ", sourceTerm=" + getSourceTerm() +
                ", targetTerm=" + getTargetTerm() +
                '}';
    }

    @XmlAccessorType(XmlAccessType.NONE)
    public static class Term {


        @XmlElement(name = "lang_name")
        private String langName;

        @XmlElement(name = "termContent")
        private String termContent;

        @XmlAttribute(name = "isSrc")
        private boolean src;

        Term() {
        }

        public boolean isSrc() {
            return src;
        }

        public void setSrc(boolean src) {
            this.src = src;
        }

        public String getLangName() {
            return langName;
        }

        public void setLangName(String langName) {
            this.langName = langName;
        }

        public String getTermContent() {
            return termContent;
        }

        public void setTermContent(String termContent) {
            this.termContent = termContent;
        }

        @Override
        public String toString() {
            return "Term{" +
                    "langName='" + langName + '\'' +
                    ", termContent='" + termContent + '\'' +
                    ", src=" + src +
                    '}';
        }
    }
}
