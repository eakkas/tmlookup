package net.globalme.gstestingtool.machinetranslation.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class AnyElement {

}
