package net.globalme.gstestingtool.machinetranslation.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@XmlAccessorType(XmlAccessType.NONE)
public class Locale implements Comparable<Locale> {

    @XmlElement
    private String code;

    @XmlElement
    private String displayName;

    public Locale() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Locale locale = (Locale) o;

        if (code != null ? !code.equals(locale.code) : locale.code != null) return false;
        if (displayName != null ? !displayName.equals(locale.displayName) : locale.displayName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Locale{" +
                "code='" + code + '\'' +
                ", displayName='" + displayName + '\'' +
                '}';
    }

    @Override
    public int compareTo(Locale o) {
        return this.getDisplayName().compareTo(o.getDisplayName());
    }
}
