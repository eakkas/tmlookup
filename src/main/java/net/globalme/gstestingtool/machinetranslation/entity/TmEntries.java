package net.globalme.gstestingtool.machinetranslation.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@XmlRootElement(name = "entries")
@XmlAccessorType(XmlAccessType.NONE)
public class TmEntries {
    @XmlElement(name = "entry")
    private List<TmEntry> tmEntries;

    public TmEntries() {
    }

    public List<TmEntry> getTmEntries() {
        return tmEntries;
    }

    public void setTmEntries(List<TmEntry> tmEntries) {
        this.tmEntries = tmEntries;
    }
}
