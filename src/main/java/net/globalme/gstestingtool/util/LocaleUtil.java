package net.globalme.gstestingtool.util;

import net.globalme.gstestingtool.machinetranslation.entity.Locale;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class LocaleUtil {

    private final static Map<String, String> languageMappings;

    private LocaleUtil() {
        //no instances
    }

    //todo refactor this
    public static Locale renameLanguageIfNeeded(final Locale locale) {
        final String displayName = locale.getDisplayName();
        final String code = displayName.substring(displayName.indexOf('[') + 1, displayName.length() - 1);
        if (languageMappings.containsKey(code)) {
            final Locale newLocale = new Locale();
            newLocale.setDisplayName(displayName.replace(displayName.substring(0, displayName.indexOf("(") - 1),
                    languageMappings.get(code)));
            newLocale.setCode(locale.getCode());
            return newLocale;
        }
        return locale;
    }

    static {
        languageMappings = new HashMap<String, String>();
        languageMappings.put("cs_CZ", "Czech");
        languageMappings.put("ja_JP", "Japanese");
        languageMappings.put("de_DE", "German");
        languageMappings.put("ru_RU", "Russian");
        languageMappings.put("it_IT", "Italian");
        languageMappings.put("zh_TW", "T.Chinese");
        languageMappings.put("da_DK", "Danish");
        languageMappings.put("nl_NL", "Dutch");
        languageMappings.put("sv_SE", "Swedish");
        languageMappings.put("fr_FR", "French - France");
        languageMappings.put("th_TH", "Thai");
        languageMappings.put("fr_CA", "French - Canada");
        languageMappings.put("en_US", "English - US");
        languageMappings.put("ko_KR", "Korean");
        languageMappings.put("zh_CN", "S.Chinese");
    }

}
