package net.globalme.gstestingtool.entity;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * The {@code Term}  class represents a term received from the term-based memory.
 *
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Term {

    private String source;

    private String translation;

    private String locale;

    private String partOfSpeech;

    private String domain;

    private String context;

    private String definition;

    public Term() {
    }

    public Term(String source, String translation, String locale, String partOfSpeech, String domain, String context, String definition) {
        this.source = source;
        this.translation = translation;
        this.locale = locale;
        this.partOfSpeech = partOfSpeech;
        this.domain = domain;
        this.context = context;
        this.definition = definition;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }


    public String getPartOfSpeech() {
        return partOfSpeech;
    }

    public void setPartOfSpeech(String partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    @Override
    public String toString() {
        return "Term{" +
                "source='" + source + '\'' +
                ", translation='" + translation + '\'' +
                ", locale='" + locale + '\'' +
                ", partOfSpeech='" + partOfSpeech + '\'' +
                ", domain='" + domain + '\'' +
                ", context='" + context + '\'' +
                ", definition="+ definition + '\'' +
                '}';
    }
}
