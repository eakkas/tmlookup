package net.globalme.gstestingtool.entity;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * The {@code TranslationMemory} class represents a hint from the translation memory.
 *
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TranslationMemory {

    private String source;

    private String translation;

    private double percentage;

    private String locale;

    private String sourceLocale;

    public TranslationMemory() {
    }

    public TranslationMemory(String source, String translation, final String locale, double percentage,
                             String sourceLocale) {
        this.source = source;
        this.translation = translation;
        this.percentage = percentage;
        this.locale = locale;
        this.sourceLocale = sourceLocale;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getSourceLocale() {
        return sourceLocale;
    }

    public void setSourceLocale(String sourceLocale) {
        this.sourceLocale = sourceLocale;
    }

    @Override
    public String toString() {
        return "TranslationMemory{" +
                "source='" + source + '\'' +
                ", translation='" + translation + '\'' +
                ", percentage=" + percentage +
                ", locale='" + locale + '\'' +
                ", sourceLocale='" + sourceLocale + '\'' +
                '}';
    }

    public String toChangeRequest(final String creationDate, final String creationId, final String changeId) {
        return "\n<tu>\n" +
                "<tuv xml:lang=\"" + sourceLocale + "\" creationdate=\"" + creationDate + "\" creationid=\"" + creationId + "\" " +
                "changeid=\"" + changeId + "\" >\n" +
                "<seg>" + source + "</seg>\n" +
                "</tuv>\n" +
                "<tuv xml:lang=\"" + locale + "\" creationdate=\"" + creationDate + "\" creationid=\"" + creationId + "\" " +
                "changeid=\"" + changeId + "\"n" +
                "<seg>" + translation + "</seg>\n" +
                "</tuv>\n" +
                "</tu>";
    }
}
