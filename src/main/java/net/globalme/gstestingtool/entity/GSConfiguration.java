package net.globalme.gstestingtool.entity;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GSConfiguration {

    private String host;
    private Integer port;
    private String username;
    private String password;
    private String termbaseName;
    private String tmProfileName;
    private boolean allowDeletion;
    private boolean onlyEnglishInSource;
    private String allowedPermissionGroup;

    public GSConfiguration() {
    }

    public GSConfiguration(String host, Integer port, /*String username, String password,*/ String termbaseName,
                           String tmProfileName, boolean allowDeletion, boolean onlyEnglishInSource, String allowedPermissionGroup) {
        this.host = host;
        this.port = port;
//        this.username = username;
//        this.password = password;
        this.termbaseName = termbaseName;
        this.tmProfileName = tmProfileName;
        this.allowDeletion = allowDeletion;
        this.onlyEnglishInSource = onlyEnglishInSource;
        this.allowedPermissionGroup = allowedPermissionGroup;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTermbaseName() {
        return termbaseName;
    }

    public void setTermbaseName(String termbaseName) {
        this.termbaseName = termbaseName;
    }

    public String getTmProfileName() {
        return tmProfileName;
    }

    public void setTmProfileName(String tmProfileName) {
        this.tmProfileName = tmProfileName;
    }

    public boolean isAllowDeletion() {
        return allowDeletion;
    }

    public void setAllowDeletion(boolean allowDeletion) {
        this.allowDeletion = allowDeletion;
    }

    public boolean isOnlyEnglishInSource() {
        return onlyEnglishInSource;
    }

    public void setOnlyEnglishInSource(boolean onlyEnglishInSource) {
        this.onlyEnglishInSource = onlyEnglishInSource;
    }

    public String getAllowedPermissionGroup() {
        return allowedPermissionGroup;
    }

    public void setAllowedPermissionGroup(String allowedPermissionGroup) {
        this.allowedPermissionGroup = allowedPermissionGroup;
    }

    @Override
    public String toString() {
        return "GSConfiguration{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", termbaseName='" + termbaseName + '\'' +
                ", tmProfileName='" + tmProfileName + '\'' +
                ", allowDeletion=" + allowDeletion +
                ", onlyEnglishInSource=" + onlyEnglishInSource +
                ", allowedPermissionGroup" + allowedPermissionGroup +
                '}';
    }


}
