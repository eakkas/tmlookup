import net.globalme.gstestingtool.entity.GSConfiguration;
import net.globalme.gstestingtool.machinetranslation.GlobalSightClient;
import net.globalme.gstestingtool.machinetranslation.GlobalSightClientImpl;
import net.globalme.gstestingtool.machinetranslation.entity.Locale;
import net.globalme.gstestingtool.machinetranslation.entity.LocalePair;
import net.globalme.gstestingtool.machinetranslation.entity.TmEntries;
import net.globalme.gstestingtool.machinetranslation.entity.TmEntry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Igor Vodopyanov &lt;igor.vodopyanov@gmail.com&gt;
 */
public class TestTest {

    private final static GSConfiguration CONFIGURATION = new GSConfiguration("http://ts1.cmswithtms.net", 80,
             "agiletranslate", "agiletranslate", true, false, "terminology.browse");


    private final static String ENCODING_UTF_8 = "UTF-8";

    private Map<Locale, ArrayList<Locale>> availableLocales;

    private GlobalSightClient globalSightClient = new GlobalSightClientImpl();

    private String test = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n " +
            "<entries>\n" +
            "       <entry>\n" +
            "                <tm id='2'>NetSuiteMstr</tm>\n" +
            "                <percentage>77.778%</percentage>\n" +
            "                <source>\n" +
            "                        <locale>en_US</locale>\n" +
            "                        <segment><bpt i=\"0\" x=\"1\" type=\"text\"></bpt>A/P account<\n" +
            "ept i=\"0\"></ept></segment>\n" +
            "                </source>\n" +
            "                <target>\n" +
            "                        <locale>th_TH</locale>\n" +
            "                        <segment><bpt i=\"0\" x=\"1\" type=\"text\"></bpt>Hello<ep\n" +
            "t i=\"0\"></ept></segment>\n" +
            "                </target>\n" +
            "        </entry>" +
            "</entries>";

    //@Test
    public void testGsClient() throws Exception {
        final Unmarshaller unmarshaller = JAXBContext.newInstance(
                TmEntries.class).createUnmarshaller();
        ByteArrayInputStream input = new ByteArrayInputStream(test.getBytes(ENCODING_UTF_8));
        final TmEntries tmEntries = (TmEntries) unmarshaller.unmarshal(input);
        final ArrayList<TmEntry> entries = new ArrayList<TmEntry>(tmEntries.getTmEntries());
        System.out.printf("entries: %s", entries);

    }

    private static Map<Locale, ArrayList<Locale>> mergeLocales(final ArrayList<LocalePair> localePairs) {
        final Map<Locale, ArrayList<Locale>> result = new HashMap<Locale, ArrayList<Locale>>();
        for (final LocalePair localePair : localePairs) {
            if (!result.containsKey(localePair.getSourceLocale())) {
                result.put(localePair.getSourceLocale(), new ArrayList<Locale>());
            }
            result.get(localePair.getSourceLocale()).add(localePair.getTargetLocale());
        }
        return result;
    }

    private String cutWordsWithSpecialCharacters(final String source) {
        final String[] words = source.split(" ");
        final StringBuilder builder = new StringBuilder();
        for (final String word : words) {
            if (!word.contains("®")) {
                builder.append(word);
                builder.append(" ");
            }
        }
        final String result = builder.toString();
        return result.substring(0, result.length() - 1);
    }

}
